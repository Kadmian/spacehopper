package spacehopper.image;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author Volker
 */
public class ImageResourceTest {

    @Test
    public void load_images() {
        Arrays.stream(ImageResource.values())
              .peek(ir -> System.out.println("Load image resource: " + ir.name()))
              .forEach(ImageResource::load);
    }

}
