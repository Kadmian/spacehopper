package spacehopper.image;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author Volker
 */
public enum ImageResource {

    AVATAR_HUMAN("human_64.png"),
    AVATAR_HUMAN_ICON("human_16.png"),
    AVATAR_KI_EASY("ki1_64.png"),
    AVATAR_KI_EASY_ICON("ki1_16.png"),
    AVATAR_KI_MEDIUM("ki2_64.png"),
    AVATAR_KI_MEDIUM_ICON("ki2_16.png"),
    AVATAR_KI_HARD("ki3_64.png"),
    AVATAR_KI_HARD_ICON("ki3_16.png"),
    AVATAR_NETWORK("network_64.png"),
    AVATAR_NETWORK_ICON("network_16.png"),
    BOARD_SQUARE("brett_quadrat_650.jpg"),
    BOARD_SQUARE_ICON("brett_quadrat_64.png"),
    BOARD_STAR("brett_stern_650.jpg"),
    BOARD_STAR_ICON("brett_stern_64.png"),
    INFO_AND_STATUS_BACKGROUND("infoAndStatusBackground.gif"),
    PATH_MARKER("markierung.png"),
    UFO_BLUE("ufoani-blue.gif"),
    UFO_BLUE_MARKED("ufo-blue-marked.gif"),
    UFO_BLUE_ICON("ufo-blue-16.png"),
    UFO_GREEN("ufoani-green.gif"),
    UFO_GREEN_MARKED("ufo-green-marked.gif"),
    UFO_GREEN_ICON("ufo-green-16.png"),
    UFO_RED("ufoani-red.gif"),
    UFO_RED_MARKED("ufo-red-marked.gif"),
    UFO_RED_ICON("ufo-red-16.png"),
    UFO_YELLOW("ufoani-yellow.gif"),
    UFO_YELLOW_MARKED("ufo-yellow-marked.gif"),
    UFO_YELLOW_ICON("ufo-yellow-16.png");

    private final String imagePath;

    ImageResource(String imagePath) {
        this.imagePath = imagePath;
    }

    public ImageIcon loadImageIcon() {
        return new ImageIcon(load());
    }

    public Image load() {
        return Toolkit.getDefaultToolkit().getImage(imageURL());
    }

    private URL imageURL() {
        return getClass().getResource(imagePath());
    }

    private String imagePath() {
        return imagePath;
    }
}
