package spacehopper.infrastructure.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.util.Arrays;

import static javax.swing.KeyStroke.getKeyStroke;

/**
 * @author Volker
 */
public class ComponentFactory {

    public static JMenuBar menuBar(JMenu... menus) {
        JMenuBar menuBar = new JMenuBar();
        applyDefaultColors(menuBar);
        menuBar.setBorder(null);
        Arrays.stream(menus).forEach(menuBar::add);
        return menuBar;
    }

    public static void applyDefaultColors(JComponent component) {
        component.setBackground(Color.BLACK);
        component.setForeground(Color.WHITE);
    }

    public static JMenuItem menuItem(String text,
                                     ActionListener actionListener,
                                     char shortCutCharacter) {
        JMenuItem menuItem = menuItem(text, actionListener);
        menuItem.setAccelerator(getKeyStroke(shortCutCharacter, InputEvent.CTRL_MASK));
        return menuItem;
    }

    public static JMenu menu(String text, JComponent... menuComponents) {
        JMenu menu = new JMenu(text);
        applyDefaultColors(menu);
        Arrays.stream(menuComponents).forEach(menu::add);
        return menu;
    }

    public static JSeparator separator() {
        return new JSeparator();
    }

    public static JMenuItem menuItem(String text, ActionListener actionListener) {
        JMenuItem menuItem = new JMenuItem(text);
        applyDefaultColors(menuItem);
        menuItem.addActionListener(actionListener);
        return menuItem;
    }

    public static JLabel label() {
        JLabel label = new JLabel();
        applyDefaultColors(label);
        return label;
    }
}
