package spacehopper.infrastructure.ui;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * @author Volker
 */
public class StyleGuide {

    private StyleGuide() {
        // Hide utility class constructor
    }

    public static TitledBorder titledBorder(String title) {
        return new TitledBorder(null,
                                title,
                                TitledBorder.DEFAULT_JUSTIFICATION,
                                TitledBorder.DEFAULT_POSITION,
                                borderTitleFont(),
                                Color.WHITE);
    }

    private static Font borderTitleFont() {
        return new Font("Dialog", Font.PLAIN, 11);
    }

    public static JPanel defaultPanel(Border border) {
        JPanel panel = defaultPanel();
        panel.setBorder(border);
        return panel;
    }

    public static JPanel defaultPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(null);
        applyDefaultColors(panel);
        return panel;
    }

    public static JButton createButton(String buttonText, ActionListener action) {
        JButton button = new JButton();
        applyDefaultColors(button);
        button.setText(buttonText);
        button.addActionListener(action);
        return button;
    }

    public static void applyDefaultColors(Component component) {
        component.setBackground(Color.BLACK);
        component.setForeground(Color.WHITE);
    }

    public static JLabel label(String text) {
        JLabel label = new JLabel(text);
        applyDefaultColors(label);
        return label;
    }

    public static JRadioButton radioButton(String label, ActionListener action) {
        JRadioButton radio = new JRadioButton();
        applyDefaultColors(radio);
        radio.setText(label);
        radio.addActionListener(action);
        return radio;
    }

    public static <E> JComboBox<E> comboBox(List<E> initialItems) {
        UpdatableComboBoxModel<E> model = new UpdatableComboBoxModel<>(initialItems);
        JComboBox<E> comboBox = new JComboBox<>(model);
        applyDefaultColors(comboBox);
        return comboBox;
    }

}