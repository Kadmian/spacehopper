package spacehopper.infrastructure.ui.table;

/**
 * @author Volker
 */
@FunctionalInterface
public interface ValueAccessor<T> {
    Object getValue(T target);
}
