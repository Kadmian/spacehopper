package spacehopper.infrastructure.ui.table;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Volker
 */
public class Table<T> {

    private final Map<Integer, Column<T>> columns = new HashMap<>();

    public Table<T> addColumn(String name, ValueAccessor<T> valueAccessor) {
        columns.putIfAbsent(columns.size(), new Column<T>(name, valueAccessor));
        return this;
    }

    public Column<T> columnAt(int columnIndex) {
        return columns.getOrDefault(columnIndex, new Column<>("", ignored -> null));
    }

    public int numberOfColumns() {
        return columns.size();
    }
}
