package spacehopper.infrastructure.ui.table;

/**
 * @author Volker
 */
public class Column<T> {

    private final String name;
    private final ValueAccessor<T> valueAccessor;

    public Column(String name, ValueAccessor<T> valueAccessor) {
        this.name = name;
        this.valueAccessor = valueAccessor;
    }

    public String name() {
        return name;
    }

    public Object valueOf(T rowItem) {
        return valueAccessor.getValue(rowItem);
    }

}
