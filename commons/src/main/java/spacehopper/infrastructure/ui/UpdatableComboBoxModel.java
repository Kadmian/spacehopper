package spacehopper.infrastructure.ui;

import javax.swing.*;
import java.util.List;
import java.util.Vector;

public class UpdatableComboBoxModel<E> extends DefaultComboBoxModel<E> {

    private List<E> items;

    public UpdatableComboBoxModel(List<E> items) {
        super(new Vector<>(items));

        this.items = List.copyOf(items);
    }

    public void update(List<E> newItems) {
        E selectedItem = (E) getSelectedItem();
        this.items = List.copyOf(newItems);
        super.removeAllElements();
        super.addAll(items);
        if (items.contains(selectedItem)) {
            super.setSelectedItem(selectedItem);
        } else {
            super.setSelectedItem(getElementAt(0));
        }
    }

}
