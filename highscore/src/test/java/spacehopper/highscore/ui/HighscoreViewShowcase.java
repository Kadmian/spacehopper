package spacehopper.highscore.ui;

import org.junit.Test;
import spacehopper.highscore.domain.HighscoreRepository;
import spacehopper.highscore.domain.Highscoreliste;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class HighscoreViewShowcase {

    private final Object LOCK = new Object();

    @Test
    public void show_view() throws InterruptedException {
        HighscoreRepository repository = new HighscoreRepository() {
            @Override
            public void save(Highscoreliste highscoreliste) {

            }

            @Override
            public Highscoreliste loadAll() {
                return new Highscoreliste();
            }
        };
        HighscoreView view = new HighscoreView(repository);
        view.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                synchronized (LOCK) {
                    LOCK.notify();
                }
            }
        });
        EventQueue.invokeLater(() -> view.show(0, 1));
        synchronized (LOCK) {
            LOCK.wait();
        }
    }

}