package spacehopper.highscore.domain;

import java.io.Serializable;

public class Highscore implements Serializable {

    private final String spielername;
    private final int anzahlZuege;

    public Highscore(String spielername, int anzahlZuege) {
        this.spielername = spielername;
        this.anzahlZuege = anzahlZuege;
    }

    public String spielerName() {
        return spielername;
    }

    public int anzahlZuege() {
        return anzahlZuege;
    }
}
