package spacehopper.highscore.domain;

import java.util.List;

public interface HighscoreRepository {

    default List<Highscore> getHighscores(int spielfeldTyp, int anzahlSpieler) {
        return loadAll().getHighscores(spielfeldTyp, anzahlSpieler);
    }

    Highscoreliste loadAll();

    void save(Highscoreliste highscoreliste);

}
