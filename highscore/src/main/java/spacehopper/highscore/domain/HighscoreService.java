package spacehopper.highscore.domain;

/**
 * @author Volker
 */
public class HighscoreService {

    private HighscoreRepository repository;

    public HighscoreService(HighscoreRepository repository) {
        this.repository = repository;
    }

    public void neuesSpielergebnis(int spielfeldTyp,
                                   int anzahlSpieler,
                                   String nameDesGewinners,
                                   int anzahlBenoetigterZuege) {
        Highscoreliste highscoreliste = repository.loadAll();
        highscoreliste.aktualisieren(spielfeldTyp,
                                     anzahlSpieler,
                                     nameDesGewinners,
                                     anzahlBenoetigterZuege);
        repository.save(highscoreliste);
    }

}
