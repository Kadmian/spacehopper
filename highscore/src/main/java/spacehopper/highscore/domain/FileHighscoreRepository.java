package spacehopper.highscore.domain;

import java.io.*;

/**
 * @author Volker
 */
public class FileHighscoreRepository implements HighscoreRepository {

    static {
        new File(System.getProperty("user.home", ".")
                 + File.separator
                 + ".spacehopper/Highscore").mkdirs();
    }

    @Override
    public void save(Highscoreliste highscoreliste) {
        try (FileOutputStream fos = new FileOutputStream(System.getProperty("user.home",
                                                                            ".")
                                                         + File.separator
                                                         + ".spacehopper/Highscore"
                                                         + "/highscore.dat");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(highscoreliste);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Highscoreliste loadAll() {
        Highscoreliste result = new Highscoreliste();
        try (FileInputStream fis = new FileInputStream(System.getProperty("user.home",
                                                                          ".")
                                                       + File.separator
                                                       + ".spacehopper/Highscore"
                                                       + "/highscore.dat");
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            result = (Highscoreliste) ois.readObject();
        } catch (FileNotFoundException e) {
            result = new Highscoreliste();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
