package spacehopper.highscore.ui;

import spacehopper.highscore.domain.Highscore;
import spacehopper.infrastructure.ui.table.Table;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class HighscoreTableModel extends AbstractTableModel {

    private final Table<Highscore> table = new Table<>();
    private final List<Highscore> data = new ArrayList<>(0);

    public HighscoreTableModel() {
        this.table.addColumn("Spieler", Highscore::spielerName)
                  .addColumn("Zuege", Highscore::anzahlZuege);
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return table.numberOfColumns();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex < data.size()) {
            Highscore highscore = data.get(rowIndex);
            return table.columnAt(columnIndex).valueOf(highscore);
        }
        return null;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return table.columnAt(columnIndex).name();
    }

    public void setHighscores(List<Highscore> data) {
        this.data.clear();
        this.data.addAll(data);
        fireTableDataChanged();
    }

}
