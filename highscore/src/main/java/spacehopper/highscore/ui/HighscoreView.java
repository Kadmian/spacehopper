/*
 * Highscore.java
 *
 * Created on March 9, 2004, 3:25 PM
 */

package spacehopper.highscore.ui;

import spacehopper.highscore.domain.Highscore;
import spacehopper.highscore.domain.HighscoreRepository;
import spacehopper.infrastructure.ui.StyleGuide;
import spacehopper.infrastructure.ui.UpdatableComboBoxModel;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class HighscoreView extends JFrame {

    public static final String ARIAL = "Arial";
    private static final int SPIELFELDTYP_STERN = 0;
    private static final int SPIELFELDTYP_QUADRAT = 1;

    private final HighscoreRepository highscoreRepository;

    private final HighscoreTableModel highscoreTableModel;

    public HighscoreView(HighscoreRepository highscoreRepository) {
        super("Highscores");
        this.highscoreRepository = highscoreRepository;
        highscoreTableModel = new HighscoreTableModel();

        initComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JPanel mainPanel = StyleGuide.defaultPanel();

        JLabel spielfeldTypLabel = StyleGuide.label("Spielfeld-Typ:");

        JComboBox<Integer> spielfeldTyp = new JComboBox<>();
        StyleGuide.applyDefaultColors(spielfeldTyp);
        spielfeldTyp.setModel(new DefaultComboBoxModel<>(new Integer[]{0, 1}));
        spielfeldTyp.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list,
                                                          Object value,
                                                          int index,
                                                          boolean isSelected,
                                                          boolean cellHasFocus) {
                Object valueToRender = value;
                if (value instanceof Integer typ) {
                    valueToRender = typ == SPIELFELDTYP_STERN ? "Stern" : "Quadrat";
                }
                return super.getListCellRendererComponent(list,
                                                          valueToRender,
                                                          index,
                                                          isSelected,
                                                          cellHasFocus);
            }
        });


        JLabel anzahlSpielerLabel = StyleGuide.label("Anzahl Spieler:");
        JComboBox<Integer> anzahlSpieler = StyleGuide.comboBox(List.of(1, 2, 3));

        JPanel filterPanel = StyleGuide.defaultPanel();
        filterPanel.add(anzahlSpielerLabel);
        anzahlSpielerLabel.setBounds(240, 0, 100, 20);
        filterPanel.add(spielfeldTypLabel);
        spielfeldTypLabel.setBounds(0, 0, 100, 20);
        filterPanel.add(spielfeldTyp);
        spielfeldTyp.setBounds(115, 0, 100, 20);
        filterPanel.add(anzahlSpieler);
        anzahlSpieler.setBounds(340, 0, 50, 20);

        mainPanel.add(filterPanel);
        filterPanel.setBounds(15, 15, 390, 20);

        spielfeldTyp.addActionListener(evt -> {
            Integer typ = (Integer) spielfeldTyp.getSelectedItem();
            ((UpdatableComboBoxModel<Integer>) anzahlSpieler.getModel()).update(List.of(1,
                                                                                        2,
                                                                                        3
                                                                                        + typ));

            updateHighscores(typ, (Integer) anzahlSpieler.getSelectedItem());
        });
        anzahlSpieler.addActionListener(evt -> {
            Integer typ = (Integer) spielfeldTyp.getSelectedItem();
            Integer spielerCount = (Integer) anzahlSpieler.getSelectedItem();
            if (typ != null && spielerCount != null) {
                updateHighscores(typ, spielerCount);
            }
        });

        JTable highscoreTable = new JTable();
        highscoreTable.setFont(new Font(ARIAL, Font.BOLD, 18));
        highscoreTable.setModel(highscoreTableModel);
        highscoreTable.setRowHeight(32);
        JScrollPane tableScrollpane = new JScrollPane(highscoreTable);
        mainPanel.add(tableScrollpane);
        tableScrollpane.setBounds(15, 45, 390, 350);

        getContentPane().add(mainPanel, BorderLayout.CENTER);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width - 420) / 2, (screenSize.height - 410) / 2, 435, 450);
    }

    private void updateHighscores(int spielfeldTyp, int anzahlSpieler) {
        List<Highscore> highscores = getHighscores(spielfeldTyp, anzahlSpieler);
        highscoreTableModel.setHighscores(highscores);
    }

    private List<Highscore> getHighscores(int spielfeldTyp, int anzahlSpieler) {
        return highscoreRepository.getHighscores(spielfeldTyp, anzahlSpieler);
    }

    public void show(int spielfeldTyp, int anzahlSpieler) {
        initializeData(spielfeldTyp, anzahlSpieler);
        setVisible(true);
    }

    private void initializeData(int spielfeldTyp, int anzahlSpieler) {
        updateHighscores(spielfeldTyp, anzahlSpieler);

        // Setze SpielfeldTyp und AnzahlSpieler in den ComboBoxen
    }

}
