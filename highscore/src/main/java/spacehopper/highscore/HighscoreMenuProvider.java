package spacehopper.highscore;

import spacehopper.highscore.domain.FileHighscoreRepository;
import spacehopper.highscore.ui.HighscoreView;
import spacehopper.infrastructure.ui.ComponentFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;

import static spacehopper.infrastructure.ui.ComponentFactory.menuItem;

/**
 * @author Volker
 */
public class HighscoreMenuProvider {

    public JMenu get() {
        JMenuItem highscoreMenuItem = menuItem("Highscore anzeigen",
                                               this::showHighscores);
        return ComponentFactory.menu("HighScore", highscoreMenuItem);
    }

    private void showHighscores(ActionEvent evt) {
        new HighscoreView(new FileHighscoreRepository()).show(0, 1);
    }

}
