package spacehopper.help;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

class HTMLPanel extends JPanel {

    private JEditorPane htmlPane;

    HTMLPanel(String initialPageURL) {
        try {
            URL url = getClass().getResource(initialPageURL);
            htmlPane = new JEditorPane(url);
            htmlPane.setEditable(false);
            htmlPane.addHyperlinkListener(switchPageListener());

            JScrollPane scrollPane = new JScrollPane(htmlPane);
            setLayout(new BorderLayout());
            add(scrollPane, BorderLayout.CENTER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public HyperlinkListener switchPageListener() {
        return e -> {
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                switchPage(e.getURL());
            }
        };
    }

    private void switchPage(URL url) {
        try {
            htmlPane.setPage(url);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void goTo(String pageurl) {
        URL url = getClass().getResource(pageurl);
        switchPage(url);
    }

}
