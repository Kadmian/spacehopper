package spacehopper.help;

import spacehopper.infrastructure.ui.ComponentFactory;

import javax.swing.*;

import static spacehopper.infrastructure.ui.ComponentFactory.menuItem;

/**
 * @author Volker
 */
public class HilfeMenuProvider {

    public JMenu get() {
        JMenuItem spielregelnMenuItem = menuItem("Spielregeln",
                                                 evt -> HilfeBrowser.showHelp());
        JMenuItem infoMenuItem = menuItem("Info", evt -> HilfeBrowser.showInfo());
        return ComponentFactory.menu("Hilfe", spielregelnMenuItem, infoMenuItem);
    }

}
