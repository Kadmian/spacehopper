package spacehopper.help;
/*
 * HelpBrowser.java
 *
 * Created on 19. März 2004, 23:44
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class HilfeBrowser extends JFrame {

    public static final String INDEX = "index.html";
    public static final String SPIELREGELN = "spielregeln.html";
    public static final String INFO = "info.html";

    private HTMLPanel htmlPanel;

    private HilfeBrowser(String initialPageURL) {
        super("Hilfe");

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        initComponents(initialPageURL);
    }

    private void initComponents(String initialPageURL) {
        htmlPanel = new HTMLPanel(initialPageURL);
        getContentPane().add(htmlPanel, BorderLayout.CENTER);

        JButton uebersichtButton = createButton("\u00dcbersicht", this::showIndex);
        JButton schliessenButton = createButton("Schlie\u00dfen", this::closeBrowser);

        JPanel helpPanel = new JPanel();
        helpPanel.add(uebersichtButton);
        helpPanel.add(schliessenButton);
        getContentPane().add(helpPanel, BorderLayout.SOUTH);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width - 640) / 2, (screenSize.height - 480) / 2, 640, 480);
    }

    private JButton createButton(String text, ActionListener actionListener) {
        JButton button = new JButton();
        button.setText(text);
        button.addActionListener(actionListener);
        return button;
    }

    private void showIndex(ActionEvent evt) {
        htmlPanel.goTo(INDEX);
    }

    public static void showInfo() {
        show(INFO);
    }

    public static void showHelp() {
        show(SPIELREGELN);
    }

    private static void show(String spielregeln) {
        HilfeBrowser hilfeBrowser = new HilfeBrowser(spielregeln);
        hilfeBrowser.setVisible(true);
    }

    private void closeBrowser(ActionEvent evt) {
        EventQueue.invokeLater(() -> this.dispatchEvent(new WindowEvent(this,
                                                                        WindowEvent.WINDOW_CLOSING)));
    }

}
