package spacehopper.app;

import spacehopper.core.SpielStatus;
import spacehopper.core.Spielverwaltung;
import spacehopper.highscore.domain.FileHighscoreRepository;
import spacehopper.highscore.domain.HighscoreService;
import spacehopper.highscore.ui.HighscoreView;

import java.awt.*;

/**
 * @author Volker
 */
public class SpielEndeListener implements Spielverwaltung.Listener {

    @Override
    public void spielBeendet(SpielStatus update) {
        //        if (!update.getCheater()) {
            fuegeSpielergebnisZuHighscoreHinzu(update);
            zeigeHighscoresAn(update);
        //        }
    }

    private void fuegeSpielergebnisZuHighscoreHinzu(SpielStatus update) {
        HighscoreService highscoreService =
                new HighscoreService(new FileHighscoreRepository());
        highscoreService.neuesSpielergebnis(update.spielfeldTyp().legacyType(),
                                            update.getSpielerNamen().length,
                                            update.getSpielerNamen()[update.getAktiverSpielerIndex()],
                                            update.getRunde());
    }

    private void zeigeHighscoresAn(SpielStatus update) {
        EventQueue.invokeLater(showHighscore(update.spielfeldTyp().legacyType(),
                                             update.getSpielerNamen().length));
    }

    private Runnable showHighscore(int spielfeldTyp, int anzahlSpieler) {
        return () -> {
            HighscoreView highscoreView =
                    new HighscoreView(new FileHighscoreRepository());
            highscoreView.show(spielfeldTyp, anzahlSpieler);
        };
    }

}
