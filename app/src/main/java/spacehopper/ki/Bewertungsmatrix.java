package spacehopper.ki;

import spacehopper.core.Position;

import java.io.Serializable;

public class Bewertungsmatrix implements Serializable {

    private final int[][][] matrix;

    public Bewertungsmatrix(int[][][] matrix) {
        this.matrix = matrix;
    }

    public int getBewertung(int hausIndex, Position position) {
        return getBewertung(hausIndex, position.x(), position.y());
    }

    public int getBewertung(int hausIndex, int x, int y) {
        return matrix[hausIndex][x][y];
    }
}

