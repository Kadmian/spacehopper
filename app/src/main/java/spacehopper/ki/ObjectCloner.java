package spacehopper.ki;

import java.io.*;

final class ObjectCloner {

    private ObjectCloner() {
        // Hide utility class constructor
    }

    @SuppressWarnings("unchecked")
    public static <T extends Serializable> T cloneObject(T object) throws Exception {
        byte[] objectBytes = serialize(object);
        return (T) deserialize(objectBytes, object.getClass());
    }

    private static <T extends Serializable> byte[] serialize(T oldObject)
            throws IOException {
        try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
             ObjectOutput objectOutput = new ObjectOutputStream(byteStream)) {
            objectOutput.writeObject(oldObject);
            objectOutput.flush();
            return byteStream.toByteArray();
        }
    }

    private static <T extends Serializable> T deserialize(byte[] objectBytes,
                                                          Class<T> objectClass)
            throws IOException, ClassNotFoundException {
        try (InputStream byteStream = new ByteArrayInputStream(objectBytes);
             ObjectInput objectInput = new ObjectInputStream(byteStream)) {
            return objectClass.cast(objectInput.readObject());
        }
    }

}
