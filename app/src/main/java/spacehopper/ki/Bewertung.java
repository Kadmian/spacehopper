package spacehopper.ki;

import spacehopper.core.Position;
import spacehopper.core.Spieler;
import spacehopper.core.Spielfigur;
import spacehopper.core.Zug;

import java.util.List;

public abstract class Bewertung {

    private final Bewertungsmatrix bewertungsmatrix;

    public Bewertung(Bewertungsmatrix bewertungsmatrix) {
        this.bewertungsmatrix = bewertungsmatrix;
    }

    public int bewerteZug(Zug zug, int spielerHausIndex) {
        Position start = zug.startpunkt();
        Position ziel = zug.endpunkt();
        return bewerte(ziel, spielerHausIndex) - bewerte(start, spielerHausIndex);
    }

    private int bewerte(Position position, int spielerHausIndex) {
        return bewertungsmatrix.getBewertung(spielerHausIndex, position);
    }

    public int bewerteSpielerPosition(Spieler spieler, int spielerHausIndex) {
        List<Spielfigur> tmpFiguren = spieler.spielfiguren();
        return tmpFiguren.stream().map(Spielfigur::position)
                         .mapToInt(position -> bewerte(position, spielerHausIndex))
                         .sum();
    }

    public abstract int getMaxBewertung();

    protected int bewerte(int x, int y) {
        return bewertungsmatrix.getBewertung(0, x, y);
    }

}
