package spacehopper.ki;

import spacehopper.core.*;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class KIRunner {

    private static final int CLICK_DELAY = 100;

    private final Spielfeld spielfeld;
    private final Spieler[] spielerArray;
    private final int selfIndex;
    private final int staerke;
    private final Spielverwaltung sv;

    public KIRunner(SpielController currentGame, Spielverwaltung sv) throws Exception {
        this.sv = sv;
        SpielController spielClone = ObjectCloner.cloneObject(currentGame);

        this.spielfeld = spielClone.getSpielfeld();
        this.spielerArray = spielClone.getTeilnehmer();
        this.selfIndex = spielClone.getAktiverSpielerIndex();
        Spieler self = spielerArray[spielClone.getAktiverSpielerIndex()];
        this.staerke = self.typ() == SpielerTyp.HUMAN
                       ? SpielerTyp.COMPUTER_MEDIUM.legacyType()
                       : self.typ().legacyType();
    }

    public void schlageZugVor() {
        execute(new PerformMove(positionen -> sv.gibKontrolleZurueck()));
    }

    public void fuehreZugAus() {
        execute(new PerformMove(this::schliesseZugAb));
    }

    private void execute(Runnable runnable) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(runnable);
        executor.shutdown();
    }

    private Zug berechneZug() {
        KIEngine engine =
                new DefaultKIEngine(spielfeld, selfIndex, staerke, List.of(spielerArray));
        return engine.berechneZug();
    }

    private void ziehe(List<Position> positionen) {
        for (Position position : positionen) {
            klickeFeld(position);
            try {
                Thread.sleep(CLICK_DELAY);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void klickeFeld(Position position) {
        sv.klickeFeld(SpielerTyp.byLegacyType(staerke).orElse(SpielerTyp.COMPUTER_MEDIUM),
                      position);
    }

    private void schliesseZugAb(List<Position> positionen) {
        Position letztePosition = positionen.get(positionen.size() - 1);
        klickeFeld(letztePosition);
    }

    private class PerformMove implements Runnable {

        private final MoveFinalizer finalizer;

        public PerformMove(MoveFinalizer finalizer) {
            this.finalizer = finalizer;
        }

        @Override
        public void run() {
            Zug zug = berechneZug();

            List<Position> positionen = sv.erreichbar(zug);

            ziehe(positionen);

            finalizer.finalizeMove(positionen);
        }
    }

    private interface MoveFinalizer {
        void finalizeMove(List<Position> positionen);
    }

}
