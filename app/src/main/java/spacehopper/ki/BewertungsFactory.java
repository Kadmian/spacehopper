package spacehopper.ki;

import spacehopper.core.SpielfeldTyp;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BewertungsFactory {

    static {
        ensureFolderExists(kiFolder());
        for (SpielfeldTyp spielfeldTyp : SpielfeldTyp.values()) {
            if (!Files.exists(matrixSaveFile(spielfeldTyp))) {
                erzeugeMatrixSaveFile(spielfeldTyp);
            }
        }
    }

    private static Path kiFolder() {
        return Paths.get(userHome(),".spacehopper", "KI");
    }

    private static void erzeugeMatrixSaveFile(SpielfeldTyp spielfeldTyp) {
        int[][][] matrizen;
        if (spielfeldTyp == SpielfeldTyp.STERN) {
            matrizen = MatrixBerechnung.berechneSternMatrizen();
        } else {
            matrizen = MatrixBerechnung.berechneQuadratMatrizen();
        }
        speichereMatrix(matrizen, matrixSaveFile(spielfeldTyp));
    }

    public static Bewertung neueBewertung(SpielfeldTyp spielfeldTyp, int anzahlSpieler) {
        Bewertungsmatrix bewertungsmatrix = ladeBewertungsMatrix(spielfeldTyp);
        if (spielfeldTyp == SpielfeldTyp.STERN) {
            return new SternBewertung(bewertungsmatrix);
        } else {
            return new QuadratBewertung(anzahlSpieler, bewertungsmatrix);
        }
    }

    private static Bewertungsmatrix ladeBewertungsMatrix(SpielfeldTyp spielfeldTyp) {
        try (InputStream fis = Files.newInputStream(matrixSaveFile(spielfeldTyp));
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            return (Bewertungsmatrix) ois.readObject();
        } catch (Exception e) {
            try {
                Files.deleteIfExists(matrixSaveFile(spielfeldTyp));
                erzeugeMatrixSaveFile(spielfeldTyp);
                return ladeBewertungsMatrix(spielfeldTyp);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            System.err.println("Keine Bewertungsfunktion vorhanden");
            e.printStackTrace(System.err);
        }
        return null;
    }

    private static Path matrixSaveFile(SpielfeldTyp spielfeldTyp) {
        return spielfeldTyp == SpielfeldTyp.STERN ? kiFolder().resolve("stern.dat") : kiFolder().resolve("quadrat.dat");
    }

    private static String userHome() {
        return System.getProperty("user.home", ".");
    }

    private static void ensureFolderExists(Path folder) {
        if (Files.notExists(folder)) {
            try {
                Files.createDirectories(folder);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void speichereMatrix(int[][][] matrix, Path matrixSaveFile) {
        try (OutputStream fos = Files.newOutputStream(matrixSaveFile);
             ObjectOutput oos = new ObjectOutputStream(fos)) {
            oos.writeObject(new Bewertungsmatrix(matrix));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
