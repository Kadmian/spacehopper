package spacehopper.ki;

public class QuadratBewertung extends Bewertung {

    private final int anzahlSpieler;

    public QuadratBewertung(int anzahlSpieler, Bewertungsmatrix bewertungsmatrix) {
        super(bewertungsmatrix);
        this.anzahlSpieler = anzahlSpieler;
    }

    @Override
    public int getMaxBewertung() {
        int ergebnis = 0;
        int anzahlSpielerDurchVier = anzahlSpieler / 4;
        for (int i = 11 + anzahlSpielerDurchVier; i < 16; i++) {
            for (int j = 14 - i + 11 + anzahlSpielerDurchVier; j < 16; j++) {
                ergebnis += bewerte(i, j);
            }
        }
        ergebnis -= bewerte(15, 10 + anzahlSpielerDurchVier);
        return ergebnis;
    }
}
