package spacehopper.ki;

import spacehopper.core.Zug;

/**
 * @author Volker
 */
public interface KIEngine {

    Zug berechneZug();

}
