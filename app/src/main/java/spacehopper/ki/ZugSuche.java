package spacehopper.ki;

import spacehopper.core.*;

import java.util.*;

public class ZugSuche {

    private final List<Spieler> alleSpieler;
    private final Spielfeld spielfeld;

    public ZugSuche(List<Spieler> alleSpieler, Spielfeld spielfeld) {
        this.alleSpieler = alleSpieler;
        this.spielfeld = spielfeld;
    }

    public Deque<Zug> such(Spieler spieler) {
        List<Position> besetztePositionen = bestimmeBesetztePositionen();

        List<Zug> result = spieler.spielfiguren()
                                  .stream()
                                  .map(spielfigur -> findeZuegeFuerFigur(
                                          besetztePositionen,
                                          spielfigur))
                                  .flatMap(Collection::stream)
                                  .toList();
        return new LinkedList<>(result);
    }

    private List<Position> bestimmeBesetztePositionen() {
        return alleSpieler.stream()
                          .flatMap(spieler -> spieler.spielfiguren().stream())
                          .map(Spielfigur::position)
                          .toList();
    }

    private Deque<Zug> findeZuegeFuerFigur(List<Position> besetztePositionenOriginal,
                                           Spielfigur spielfigur) {
        Deque<Zug> moeglicheZuege = new LinkedList<>();
        List<Position> betrachtetePositionen =
                new LinkedList<>(besetztePositionenOriginal);

        // Fuer jeden Nachbarn des Brettpunktes, auf dem die Spielfigur steht
        for (int richtung = 0; richtung < 8; richtung++) {
            // Pruefen ob der Nachbar ein gueltiger Brettpunkt ist
            Brettpunkt ausgangspunkt = spielfeld.brettpunktAn(spielfigur.position());
            Optional<Brettpunkt> direkterNachbar = ausgangspunkt.getNachbarn(richtung);
            int finalRichtung = richtung;
            Spieler spieler = spielfigur.getSpieler();

            direkterNachbar.ifPresent(nachbar -> {
                Zug basisZug = new Zug().erweitertUm(spielfigur.position());
                if (nachbar.istBesetzbar()) {
                    if (nachbar.darfBesetztWerdenVon(spieler) && richtungIstZulaessig(
                            finalRichtung)) {
                        moeglicheZuege.add(basisZug.erweitertUm(nachbar.position()));
                    }
                } else {
                    Optional<Brettpunkt> sprungzielOpt =
                            nachbar.getNachbarn(finalRichtung);
                    sprungzielOpt.ifPresent(sprungziel -> {
                        if (sprungziel.istBesetzbarDurch(spieler)) {
                            Zug sprungZug = basisZug.erweitertUm(sprungziel.position());
                            moeglicheZuege.add(sprungZug);
                            betrachtetePositionen.add(sprungziel.position());

                            springeWeiter(sprungziel,
                                          moeglicheZuege,
                                          betrachtetePositionen,
                                          sprungZug,
                                          spieler);
                        }
                    });
                }
            });
        }
        return moeglicheZuege;
    }


    private boolean richtungIstZulaessig(int finalRichtung) {
        return (spielfeld.typ() == SpielfeldTyp.STERN) || ((finalRichtung % 2) != 0);
    }

    private void springeWeiter(Brettpunkt ausgangspunkt,
                               Deque<Zug> moeglicheZuege,
                               List<Position> betrachtetePositionen,
                               Zug bisherigerZug,
                               Spieler spieler) {
        for (int richtung = 0; richtung < 8; richtung++) {
            Optional<Brettpunkt> nachbarOpt = ausgangspunkt.getNachbarn(richtung);
            int finalRichtung = richtung;
            nachbarOpt.ifPresent(nachbar -> handleNachbar(moeglicheZuege,
                                                          betrachtetePositionen,
                                                          finalRichtung,
                                                          nachbar,
                                                          spieler,
                                                          bisherigerZug));
        }
    }

    private void handleNachbar(Deque<Zug> moeglicheZuege,
                               List<Position> betrachtetePositionen,
                               int richtung,
                               Brettpunkt nachbar,
                               Spieler spieler,
                               Zug bisherigerZug) {
        if (nachbar.istBesetzt()) {
            Optional<Brettpunkt> sprungzielOpt = nachbar.getNachbarn(richtung);
            sprungzielOpt.ifPresent(sprungziel -> {
                if (sprungziel.istBesetzbarDurch(spieler)
                    && !betrachtetePositionen.contains(sprungziel.position())) {
                    Zug sprungZug = bisherigerZug.erweitertUm(sprungziel.position());
                    moeglicheZuege.add(sprungZug);
                    betrachtetePositionen.add(sprungziel.position());

                    springeWeiter(sprungziel,
                                  moeglicheZuege,
                                  betrachtetePositionen,
                                  sprungZug,
                                  spieler);
                }
            });
        }
    }


}
