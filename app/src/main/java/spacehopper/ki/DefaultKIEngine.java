package spacehopper.ki;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spacehopper.core.*;

import java.util.Deque;
import java.util.List;

/**
 * @author Volker
 */
public class DefaultKIEngine implements KIEngine {

    private static final Logger LOG =
            LoggerFactory.getLogger(DefaultKIEngine.class.getSimpleName());

    private final int spielerIndex;
    private final List<Spieler> teilnehmer;
    private final Bewertung bewertung;
    private final Spielfeld spielfeld;
    private final int siegSumme;
    private final int suchtiefe;

    public DefaultKIEngine(Spielfeld spielfeld,
                           int spielerIndex,
                           int staerke,
                           List<Spieler> teilnehmer) {
        this.spielfeld = spielfeld;
        this.teilnehmer = teilnehmer;
        this.spielerIndex = spielerIndex;
        this.suchtiefe = bestimmeSuchtiefe(staerke);
        bewertung = BewertungsFactory.neueBewertung(spielfeld.typ(), anzahlSpieler());
        siegSumme = bewertung.getMaxBewertung();
    }

    private int bestimmeSuchtiefe(int staerke) {
        int suchtiefe = staerke + 1 - (findeMoeglicheZuege(spielerIndex).size() / 75);
        if (suchtiefe < staerke) {
            suchtiefe = staerke;
        }
        return suchtiefe;
    }

    @Override
    public Zug berechneZug() {
        Deque<Zug> moeglicheZuege = findeMoeglicheZuege(spielerIndex);
        LOG.info("{} mögliche Züge gefunden", moeglicheZuege.size());
        return bestimmeBestenZug(moeglicheZuege);
    }

    private Deque<Zug> findeMoeglicheZuege(int spielerIndex) {
        ZugSuche zugSuche = new ZugSuche(teilnehmer, spielfeld);
        return zugSuche.such(teilnehmer.get(spielerIndex));
    }

    private Zug bestimmeBestenZug(Deque<Zug> nachfolger) {
        Zug besterZug = nachfolger.getFirst();
        int max = Integer.MIN_VALUE;
        Spieler aktuellerSpieler = teilnehmer.get(spielerIndex);
        int spielerHausIndex = bestimmeHausIndex(spielerIndex);
        int aktuelleSumme =
                bewertung.bewerteSpielerPosition(aktuellerSpieler, spielerHausIndex);

        for (Zug nextZug : nachfolger) {
            LOG.info("Prüfe {}", nextZug);
            int zugWertung = bewertung.bewerteZug(nextZug, spielerHausIndex);
            if (zugWertung >= -5) {
                ziehen(aktuellerSpieler, nextZug);
                int abfindWert = abfind(aktuelleSumme + zugWertung,
                                        (this.spielerIndex + 1) % anzahlSpieler(),
                                        1);
                if (abfindWert > max) {
                    besterZug = nextZug;
                    max = abfindWert;
                }
                zurueckziehen(aktuellerSpieler, nextZug);
            }
        }
        return besterZug;
    }

    private boolean suchtiefeErreicht(int tiefe) {
        return tiefe >= suchtiefe;
    }

    private int anzahlSpieler() {
        return teilnehmer.size();
    }

    private int bestimmeHausIndex(int spielerIndex) {
        return (anzahlSpieler() == 2 && spielfeld.typ() == SpielfeldTyp.QUADRAT) ?
               spielerIndex
               * 2 : spielerIndex;
    }

    private void ziehen(Spieler spieler, Zug zug) {
        if (!spielfeld.brettpunktAn(zug.startpunkt()).istBesetzt()) {
            LOG.error("Ziehen nicht möglich: keine Figur auf Startpunkt {}",
                      zug.startpunkt());
            LOG.warn("Figuren Spieler {}: {}",
                     spieler.getName(),
                     spieler.spielfiguren().stream().map(Spielfigur::position).toList());
        }
        if (spielfeld.brettpunktAn(zug.endpunkt()).istBesetzt()) {
            LOG.error("Ziehen nicht möglich: Zielpunkt an {} ist besetzt",
                      zug.startpunkt());
            LOG.warn("Figuren Spieler {}: {}",
                     spieler.getName(),
                     spieler.spielfiguren().stream().map(Spielfigur::position).toList());
        }
        spieler.figurAn(zug.startpunkt())
               .ifPresent(figur -> figur.zieheNach(spielfeld.brettpunktAn(zug.endpunkt())));
        LOG.info("Gezogen: {} -> {}", zug.startpunkt(), zug.endpunkt());
    }

    private int abfind(int aktuelleWertung, int aktuellerSpielerIndex, int tiefe) {
        if (suchtiefeErreicht(tiefe)) {
            return aktuelleWertung;
        }
        if (aktuelleWertung == siegSumme) {
            return Integer.MAX_VALUE - tiefe * 10000;
        }
        Deque<Zug> nachfolger = findeMoeglicheZuege(aktuellerSpielerIndex);
        int currentMax = Integer.MIN_VALUE;
        for (Zug nextZug : nachfolger) {
            LOG.info("Unterzug: {}", nextZug);
            int spielerHausIndex = bestimmeHausIndex(aktuellerSpielerIndex);
            int wert = bewertung.bewerteZug(nextZug, spielerHausIndex);
            if (wert < -5) {
                LOG.info("Skipped");
                continue;
            }
            ziehen(teilnehmer.get(aktuellerSpielerIndex), nextZug);
            int abfindWert = abfind(aktuelleWertung + (wert * (aktuellerSpielerIndex
                                                               == spielerIndex ? 1 : -1)),
                                    ((aktuellerSpielerIndex + 1) % anzahlSpieler()),
                                    tiefe + 1);
            currentMax = Math.max(abfindWert, currentMax);
            zurueckziehen(teilnehmer.get(aktuellerSpielerIndex), nextZug);
        }
        return currentMax;
    }

    private void zurueckziehen(Spieler spieler, Zug zug) {
        spieler.figurAn(zug.endpunkt())
               .ifPresent(figur -> figur.zieheNach(spielfeld.brettpunktAn(zug.startpunkt())));
        LOG.info("Zurückgezogen: {} -> {}", zug.endpunkt(), zug.startpunkt());
    }

}
