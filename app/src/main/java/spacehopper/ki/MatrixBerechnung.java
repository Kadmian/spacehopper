package spacehopper.ki;

import java.awt.*;

public class MatrixBerechnung {

    public static int[][][] berechneSternMatrizen() {
        int[][][] matrix = new int[3][][];
        //@formatter:off
        int[][] spielerI = {{-4},
                            {18, 18},
                            {39, 40, 39},
                            {60, 61, 61, 60},
                            {0, 0, 0, 0, 80, 81, 82, 81, 80, 0, 0, 0, 0},
                            {0, 0, 0, 100, 101, 102, 102, 101, 100, 0, 0, 0},
                            {0, 0, 119, 120, 121, 122, 121, 120, 119, 0, 0},
                            {0, 137, 138, 139, 140, 140, 139, 138, 137, 0},
                            {154, 155, 156, 157, 158, 157, 156, 155, 154},
                            {0, 173, 174, 175, 176, 176, 175, 174, 173, 0},
                            {0, 0, 191, 192, 193, 194, 193, 192, 191, 0, 0},
                            {0, 0, 0, 212, 214, 213, 214, 213, 212, 0, 0, 0},
                            {0, 0, 0, 0, 232, 234, 234, 233, 232, 0, 0, 0, 0},
                            {255, 254, 255, 254},
                            {274, 275, 274},
                            {296, 296},
                            {316}};
        //@formatter:on


        matrix[0] = spielerI;
        int[][] spielerII = {
                //@formatter:off
                {0},
                {0, 0},
                {0, 0, 0},
                {0, 0, 0, 0},
                {316, 296, 274, 255, 232, 212, 191, 173, 154, 0, 0, 0, 0},
                {296, 275, 254, 234, 214, 192, 174, 155, 137, 0, 0, 0},
                {274, 255, 234, 213, 193, 175, 156, 138, 119, 0, 0},
                {254, 233, 214, 194, 176, 157, 139, 120, 100, 0},
                {232, 213, 193, 176, 158, 140, 121, 101, 80},
                {0, 212, 192, 175, 157, 140, 122, 102, 81, 60},
                {0, 0, 191, 174, 156, 139, 121, 102, 82, 61, 39},
                {0, 0, 0, 173, 155, 138, 120, 101, 81, 61, 40, 18},
                {0, 0, 0, 0, 154, 137, 119, 100, 80, 60, 39, 18, -4},
                {0, 0, 0, 0},
                {0, 0, 0},
                {0, 0},
                {0}
                //@formatter:on
        };
        int[][] spielerIII = new int[17][];
        for (int n = 0; n < 17; n++) {
            spielerIII[n] = new int[spielerII[n].length];
            int k = 0;
            for (int m = spielerII[n].length - 1; m >= 0; m--) {
                spielerIII[n][m] = spielerII[n][k];
                k++;
            }
        }

        matrix[1] = spielerII;
        matrix[2] = spielerIII;
        return matrix;
    }

    public static int[][][] berechneQuadratMatrizen() {
        int[][][] matrix = new int[4][16][16];

        int[] basiswerte = berechneBasiswerte();
        Point farthestPoint = new Point(15, 15);
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                Point point = new Point(x, y);
                int absDiffXY = Math.abs(point.x - point.y);
                int value = berechneWert(basiswerte[point.x + point.y],
                                         point.distance(farthestPoint),
                                         absDiffXY);
                matrix[0][x][y] = value;
                matrix[1][x][15 - y] = value;
                matrix[2][15 - x][15 - y] = value;
                matrix[3][15 - x][y] = value;
            }
        }
        return matrix;
    }

    private static int[] berechneBasiswerte() {
        int[] basiswert = new int[32];
        basiswert[0] = 32 * 32;
        for (int i = 1; i < 32; i++) {
            basiswert[i] = (32 - i) * (32 - i) + basiswert[i - 1];
        }
        return basiswert;
    }

    private static int berechneWert(int i, double distance, int absDiffXY) {
        int distanzMalus1 = distance > 4.5 ? 500 : 0;
        int distanzMalus2 = distance > 3.5 ? 500 : 0;
        int malusAbweichungDiagonale = absDiffXY > 4 ? 2500 * absDiffXY : 0;
        return i - distanzMalus1 - malusAbweichungDiagonale - distanzMalus2;
    }

}
