package spacehopper.ki;

public class SternBewertung extends Bewertung {
    public SternBewertung(Bewertungsmatrix bewertungsmatrix) {
        super(bewertungsmatrix);
    }

    @Override
    public int getMaxBewertung() {
        int ergebnis = 0;
        for (int x = 13; x < 17; x++) {
            for (int y = 0; y < 17 - x; y++) {
                ergebnis += bewerte(x, y);
            }
        }
        for (int y = 4; y < 9; y++) {
            ergebnis += bewerte(12, y);
        }
        return ergebnis;
    }
}
