package spacehopper.savegames;

import spacehopper.core.SpielStatus;
import spacehopper.core.SpielfeldTyp;
import spacehopper.gui.MainPanel;
import spacehopper.gui.spielfeld.SpielfeldPanel;
import spacehopper.gui.spielfeld.SpielfeldQuadPanel;
import spacehopper.gui.spielfeld.SpielfeldSternPanel;
import spacehopper.image.ImageResource;
import spacehopper.infrastructure.ui.StyleGuide;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SpieleVerwaltenView extends JDialog {

    private final MainPanel mainPanel;
    private final DefaultListModel<String> savegames;

    private JList<String> savegamesList;

    public SpieleVerwaltenView(MainPanel mainPanel) {
        super(mainPanel, "Spielstaende laden/speichern", false);
        this.mainPanel = mainPanel;
        initComponents();
        savegames = new DefaultListModel<>();
        savegames.clear();
        String[] currentSavegames = GameStore.listGames();
        for (int i = 0; i < 10; i++) {
            if (currentSavegames[i] != null) {
                savegames.addElement(currentSavegames[i]);
            } else {
                savegames.addElement("- leer -");
            }
        }
        savegamesList.setModel(savegames);
    }

    private void initComponents() {
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                exitForm();
            }
        });

        JPanel contentPanel = createContentPanel();
        getContentPane().add(contentPanel, BorderLayout.CENTER);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width - 481) / 2, (screenSize.height - 376) / 2, 481, 376);
    }

    private JPanel createContentPanel() {
        JLabel savegamesPanelLabel = createSavegamesPanelLabel();
        JPanel savegamesPanel = createSavegamesPanel();
        JPanel buttonPanel = createButtonPanel();

        JPanel contentPanel = StyleGuide.defaultPanel();
        contentPanel.add(savegamesPanelLabel);
        savegamesPanelLabel.setBounds(10, 10, 120, 15);
        contentPanel.add(savegamesPanel);
        savegamesPanel.setBounds(10, 30, 300, 310);
        contentPanel.add(buttonPanel);
        buttonPanel.setBounds(330, 30, 130, 310);
        return contentPanel;
    }

    private JLabel createSavegamesPanelLabel() {
        JLabel savegamesPanelLabel = new JLabel();
        savegamesPanelLabel.setForeground(Color.WHITE);
        savegamesPanelLabel.setText("Spielst\u00e4nde");
        return savegamesPanelLabel;
    }

    private JPanel createSavegamesPanel() {
        savegamesList = new JList<>();
        savegamesList.setBackground(new Color(204, 204, 204));
        JScrollPane savegamesScrollPane = new JScrollPane(savegamesList);

        JPanel savegamesPanel = StyleGuide.defaultPanel(new EtchedBorder());
        savegamesPanel.add(savegamesScrollPane);
        savegamesScrollPane.setBounds(0, 0, 300, 310);
        return savegamesPanel;
    }

    private JPanel createButtonPanel() {
        JButton saveButton = createSaveButton();
        JButton loadButton = createLoadButton();
        JButton cancelButton = createCancelButton();
        JLabel jLabel2 = new JLabel();
        jLabel2.setIcon(ImageResource.UFO_RED.loadImageIcon());

        JPanel buttonPanel = StyleGuide.defaultPanel(new EtchedBorder());
        buttonPanel.add(saveButton);
        saveButton.setBounds(10, 10, 110, 25);
        buttonPanel.add(loadButton);
        loadButton.setBounds(10, 50, 110, 25);
        buttonPanel.add(cancelButton);
        cancelButton.setBounds(10, 270, 110, 25);
        buttonPanel.add(jLabel2);
        jLabel2.setBounds(50, 200, 40, 60);
        return buttonPanel;
    }

    private JButton createSaveButton() {
        return StyleGuide.createButton("Speichern", e -> saveActionPerformed());
    }

    private void saveActionPerformed() {
        if (savegamesList.getSelectedIndex() != -1) {
            String name = JOptionPane.showInputDialog(mainPanel,
                                                      "Name",
                                                      (savegames.getElementAt(
                                                                        savegamesList.getSelectedIndex())
                                                                .equals("- leer -"))
                                                      ? ""
                                                      : savegames.getElementAt(
                                                              savegamesList.getSelectedIndex()));
            if (name != null && !name.equals("")) {
                try {
                    GameStore.saveGame(mainPanel.spielverwaltung().currentGame(),
                                       savegamesList.getSelectedIndex(),
                                       name);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispose();
            }
        }
    }

    private JButton createLoadButton() {
        return StyleGuide.createButton("Laden", e -> loadGameActionPerformed());
    }

    private void loadGameActionPerformed() {
        if (savegamesList.getSelectedIndex() != -1
            && !savegames.elementAt(savegamesList.getSelectedIndex())
                         .equals("- leer -")) {
            SpielStatus spielStatus = mainPanel.spielverwaltung()
                                               .loadGame(savegamesList.getSelectedIndex());
            mainPanel.getContentPane().remove(mainPanel.spielfeld);
            mainPanel.spielfeld = createSpielfeldPanel(spielStatus);
            mainPanel.spielerInfoPanel.update(spielStatus);
            mainPanel.spielfeld.setBounds(0, 0, 650, 650);
            mainPanel.getContentPane().add(mainPanel.spielfeld);
            mainPanel.spielfeld.revalidate();
            mainPanel.spielfeld.repaint();
            mainPanel.werteUpdateAus(spielStatus);
            dispose();
        }
    }

    private SpielfeldPanel createSpielfeldPanel(SpielStatus spielStatus) {
        SpielfeldPanel spielfeldPanel;
        if (spielStatus.spielfeldTyp() == SpielfeldTyp.STERN) {
            spielfeldPanel = new SpielfeldSternPanel(mainPanel.spielverwaltung());
        } else {
            spielfeldPanel = new SpielfeldQuadPanel(mainPanel.spielverwaltung());
        }
        return spielfeldPanel;
    }

    private JButton createCancelButton() {
        return StyleGuide.createButton("Abbrechen", e -> cancelActionPerformed());
    }

    private void cancelActionPerformed() {
        dispose();
    }

    private void exitForm() {
        dispose();
    }

}
