package spacehopper.savegames;

import spacehopper.core.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author Volker
 */
public class GameStore {

    static {
        if (Files.notExists(pathToSaveGames())) {
            try {
                Files.createDirectories(pathToSaveGames());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (Files.notExists(pathToMetaFile())) {
            try {
                Files.createFile(pathToMetaFile());
                saveListOfGames(new String[10]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static SpielController loadAutosaveGame() {
        return loadGameFrom(autosaveFile());
    }

    private static SpielController loadGameFrom(Path pathToSavegameFile) {
        SpielController loadedGame;
        try {
            loadedGame = tryLoadGame(pathToSavegameFile);
        } catch (Exception e) {
            e.printStackTrace();
            loadedGame = createDefaultGame();
        }
        return loadedGame;
    }

    private static SpielController tryLoadGame(Path pathToSavegameFile)
            throws IOException, ClassNotFoundException {
        try (InputStream fis = Files.newInputStream(pathToSavegameFile);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            return (SpielController) ois.readObject();
        }
    }

    private static SpielController createDefaultGame() {
        SpielerProfil defaultProfil = new SpielerProfil("Spieler 1", SpielerTyp.HUMAN);
        Spieler defaultSpieler = new Spieler(defaultProfil);
        return new SpielController(List.of(defaultSpieler), SpielfeldTyp.STERN);
    }

    private static Path autosaveFile() {
        return pathToSaveGames().resolve("autosave.dat");
    }

    private static Path pathToSaveGames() {
        return Paths.get(workingDirectory(), ".spacehopper", "Savegames");
    }

    private static String workingDirectory() {
        return System.getProperty("user.home", ".");
    }

    public static SpielController loadGame(int slot) {
        Path pathToSavegameFile = savegameFileForSlot(slot);
        return loadGameFrom(pathToSavegameFile);
    }

    private static Path savegameFileForSlot(int slot) {
        return pathToSaveGames().resolve("savegame" + slot + ".dat");
    }

    public static void autosave(SpielController currentGame) {
        try {
            writeGameToFile(currentGame, autosaveFile());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void writeGameToFile(SpielController currentGame,
                                        Path pathToSavegameFile) throws IOException {
        try (OutputStream savegameFileStream = Files.newOutputStream(pathToSavegameFile);
             ObjectOutput savegameStream = new ObjectOutputStream(savegameFileStream)) {
            savegameStream.writeObject(currentGame);
        }
    }

    public static void saveGame(SpielController currentGame, int slot, String name) {
        try {
            updateSavegameMetaFile(slot, name);
            Path pathToSavegameFile = savegameFileForSlot(slot);

            writeGameToFile(currentGame, pathToSavegameFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void updateSavegameMetaFile(int slot, String name) throws IOException {
        String[] spielenamen = listGames();
        spielenamen[slot] = name;
        saveListOfGames(spielenamen);
    }

    private static void saveListOfGames(String[] spielenamen) throws IOException {
        try (OutputStream configFileStream = Files.newOutputStream(pathToMetaFile());
             ObjectOutput oos = new ObjectOutputStream(configFileStream)) {
            oos.writeObject(spielenamen);
        }
    }

    public static String[] listGames() {
        try (InputStream fis = Files.newInputStream(pathToMetaFile());
             ObjectInput ois = new ObjectInputStream(fis)) {
            return (String[]) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String[10];
    }

    private static Path pathToMetaFile() {
        return pathToSaveGames().resolve("config.dat");
    }
}
