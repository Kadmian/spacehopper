package spacehopper.core;

import java.util.List;

public class SpielfeldStern extends Spielfeld {

    public static final int[] ANZAHL_KNOTEN_IN_ZEILE =
            new int[]{1, 2, 3, 4, 13, 12, 11, 10, 9, 10, 11, 12, 13, 4, 3, 2, 1};
    public static final int ANZAHL_ZEILEN = ANZAHL_KNOTEN_IN_ZEILE.length;

    public static final int[][] SPIELER_START_BEGIN_INDEX =
            new int[][]{{0, 0, 0, 0, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                        {-1, -1, -1, -1, -1, -1, -1, -1, 8, 8, 8, 8, 8, -1, -1, -1, -1},
                        {-1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, -1, -1, -1, -1}};
    public static final int[][] SPIELER_START_END_INDEX =
            new int[][]{{0, 1, 2, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                        {-1, -1, -1, -1, -1, -1, -1, -1, 8, 9, 10, 11, 12, -1, -1, -1,
                         -1},
                        {-1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, -1, -1, -1, -1}};
    public static final int[][] SPIELER_ZIEL_BEGIN_INDEX =
            new int[][]{{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 4, 0, 0, 0, 0},
                        {-1, -1, -1, -1, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1},
                        {-1, -1, -1, -1, 8, 8, 8, 8, 8, -1, -1, -1, -1, -1, -1, -1, -1}};
    public static final int[][] SPIELER_ZIEL_END_INDEX =
            new int[][]{{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 8, 3, 2, 1, 0},
                        {-1, -1, -1, -1, 4, 3, 2, 1, 0, -1, -1, -1, -1, -1, -1, -1, -1},
                        {-1, -1, -1, -1, 12, 11, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1,
                         -1}};

    public SpielfeldStern(List<Spieler> teilnehmer) {
        Spieler startFuer;
        Spieler zielFuer;

        for (int zeile = 0; zeile < ANZAHL_ZEILEN; zeile++) {
            for (int knotenIndexInZeile = 0;
                 knotenIndexInZeile < ANZAHL_KNOTEN_IN_ZEILE[zeile];
                 knotenIndexInZeile++) {
                Position position = Position.of(zeile, knotenIndexInZeile);
                startFuer = null;
                zielFuer = null;
                boolean randPkt = false;

                for (int spielerIndex = 0;
                     spielerIndex < teilnehmer.size();
                     spielerIndex++) {
                    if (istStartpunktFuerSpielerMitIndex(zeile,
                                                         knotenIndexInZeile,
                                                         spielerIndex)) {
                        startFuer = teilnehmer.get(spielerIndex);
                    }
                    if (istZielpunktFuerSpielerMitIndex(zeile,
                                                        knotenIndexInZeile,
                                                        spielerIndex)) {
                        zielFuer = teilnehmer.get(spielerIndex);
                    }
                    randPkt = randPkt || istRandpunkt(zeile,
                                                      knotenIndexInZeile,
                                                      spielerIndex);
                }

                add(new Brettpunkt(startFuer, zielFuer, randPkt, position));
                if (startFuer != null) {
                    startFuer.add(new Spielfigur(startFuer, brettpunktAn(position)));
                }
            }
        }

        verbindeBrettpunkte();
    }

    private boolean istStartpunktFuerSpielerMitIndex(int zeile,
                                                     int knotenIndexInZeile,
                                                     int spielerIndex) {
        return knotenIndexInZeile >= SPIELER_START_BEGIN_INDEX[spielerIndex][zeile]
               && knotenIndexInZeile <= SPIELER_START_END_INDEX[spielerIndex][zeile];
    }

    private boolean istZielpunktFuerSpielerMitIndex(int zeile,
                                                    int knotenIndexInZeile,
                                                    int spielerIndex) {
        return knotenIndexInZeile >= SPIELER_ZIEL_BEGIN_INDEX[spielerIndex][zeile]
               && knotenIndexInZeile <= SPIELER_ZIEL_END_INDEX[spielerIndex][zeile];
    }

    private void verbindeBrettpunkte() {
        for (int y = 0; y < ANZAHL_ZEILEN; y++) {
            for (int x = 0; x < ANZAHL_KNOTEN_IN_ZEILE[y]; x++) {
                Brettpunkt brettpunkt = brettpunktAn(y, x);
                if (x > 0) {
                    brettpunkt.setNachbarn(6, brettpunktAn(y, x - 1));
                }
                if (x < ANZAHL_KNOTEN_IN_ZEILE[y] - 1) {
                    brettpunkt.setNachbarn(2, brettpunktAn(y, x + 1));
                }

                int wert = (int) ((x - ((float) ANZAHL_KNOTEN_IN_ZEILE[y] - 1) / 2) * 2);

                if (y < ANZAHL_ZEILEN - 1
                    && wert > -ANZAHL_KNOTEN_IN_ZEILE[y + 1]
                    && wert <= ANZAHL_KNOTEN_IN_ZEILE[y + 1]) {
                    int x1 =
                            ((int) ((x - ((float) ANZAHL_KNOTEN_IN_ZEILE[y] - 1) / 2) * 2)
                             - 2 + ANZAHL_KNOTEN_IN_ZEILE[y + 1]) / 2;
                    brettpunkt.setNachbarn(5, brettpunktAn(y + 1, x1));
                }
                if (y < ANZAHL_ZEILEN - 1
                    && wert > -ANZAHL_KNOTEN_IN_ZEILE[y + 1] - 2
                    && wert < ANZAHL_KNOTEN_IN_ZEILE[y + 1]) {
                    int x1 =
                            ((int) ((x - ((float) ANZAHL_KNOTEN_IN_ZEILE[y] - 1) / 2) * 2)
                             + ANZAHL_KNOTEN_IN_ZEILE[y + 1]) / 2;
                    brettpunkt.setNachbarn(3, brettpunktAn(y + 1, x1));
                }

                if (y > 0
                    && wert > -ANZAHL_KNOTEN_IN_ZEILE[y - 1]
                    && wert <= ANZAHL_KNOTEN_IN_ZEILE[y - 1]) {
                    int x1 =
                            ((int) ((x - ((float) ANZAHL_KNOTEN_IN_ZEILE[y] - 1) / 2) * 2)
                             - 2 + ANZAHL_KNOTEN_IN_ZEILE[y - 1]) / 2;
                    brettpunkt.setNachbarn(7, brettpunktAn(y - 1, x1));
                }
                if (y > 0
                    && wert > -ANZAHL_KNOTEN_IN_ZEILE[y - 1] - 2
                    && wert < ANZAHL_KNOTEN_IN_ZEILE[y - 1]) {
                    int x1 =
                            ((int) ((x - ((float) ANZAHL_KNOTEN_IN_ZEILE[y] - 1) / 2) * 2)
                             + ANZAHL_KNOTEN_IN_ZEILE[y - 1]) / 2;
                    brettpunkt.setNachbarn(1, brettpunktAn(y - 1, x1));
                }
            }
        }
    }

    private boolean istRandpunkt(int zeile, int knotenIndexInZeile, int spielerIndex) {
        return (knotenIndexInZeile == SPIELER_START_BEGIN_INDEX[spielerIndex][zeile]
                && knotenIndexInZeile > 0)
               || (knotenIndexInZeile
                   == SPIELER_START_END_INDEX[spielerIndex][zeile]
                   && knotenIndexInZeile
                      < ANZAHL_KNOTEN_IN_ZEILE[zeile] - 1)
               || (knotenIndexInZeile == SPIELER_ZIEL_BEGIN_INDEX[spielerIndex][zeile]
                   && knotenIndexInZeile > 0)
               || (knotenIndexInZeile == SPIELER_ZIEL_END_INDEX[spielerIndex][zeile]
                   && knotenIndexInZeile < ANZAHL_KNOTEN_IN_ZEILE[zeile] - 1)
               || ((zeile == 4 && knotenIndexInZeile > 4 && knotenIndexInZeile < 8) || (
                zeile == 12
                && knotenIndexInZeile > 4
                && knotenIndexInZeile < 8));
    }

    @Override
    public SpielfeldTyp typ() {
        return SpielfeldTyp.STERN;
    }
}
