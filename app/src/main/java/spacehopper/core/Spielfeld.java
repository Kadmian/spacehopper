package spacehopper.core;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toSet;

public abstract class Spielfeld implements Serializable {

    public static final int OBEN = 0;
    public static final int RECHTS_OBEN = 1;
    public static final int RECHTS = 2;
    public static final int RECHTS_UNTEN = 3;
    public static final int UNTEN = 4;
    public static final int LINKS_UNTEN = 5;
    public static final int LINKS = 6;
    public static final int LINKS_OBEN = 7;

    private final Map<Position, Brettpunkt> spielfeld = new ConcurrentHashMap<>();

    public abstract SpielfeldTyp typ();

    protected void add(Brettpunkt brettpunkt) {
        spielfeld.put(brettpunkt.position(), brettpunkt);
    }

    public Brettpunkt brettpunktAn(int x, int y) {
        return brettpunktAn(Position.of(x, y));
    }

    public Brettpunkt brettpunktAn(Position position) {
        return spielfeld.get(position);
    }

    public List<Brettpunkt> brettpunkte() {
        return List.copyOf(spielfeld.values());
    }

    public Haus startHausFuer(Spieler spieler) {
        return getHaus(brettpunkt -> brettpunkt.istStartpunktFuer(spieler));
    }

    public Haus zielHausFuer(Spieler spieler) {
        return getHaus(brettpunkt -> brettpunkt.istEndpunktFuer(spieler));
    }

    private Haus getHaus(Predicate<Brettpunkt> hausDefinition) {
        Set<Brettpunkt> brettpunkte =
                brettpunkte().stream().filter(hausDefinition).collect(toSet());
        return new Haus(brettpunkte);
    }
}
