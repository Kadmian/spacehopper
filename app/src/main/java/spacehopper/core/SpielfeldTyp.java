package spacehopper.core;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Volker
 */
public enum SpielfeldTyp {

    STERN(0),
    QUADRAT(1);

    private final int legacyType;

    SpielfeldTyp(int legacyType) {
        this.legacyType = legacyType;
    }

    public static Optional<SpielfeldTyp> byLegacyType(int legacyType) {
        return Stream.of(values())
                     .filter(spielfeldTyp -> spielfeldTyp.legacyType == legacyType)
                     .findFirst();
    }

    public int legacyType() {
        return legacyType;
    }

}
