package spacehopper.core;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


public class Zug implements Serializable {

    private final LinkedList<Position> wegpunkte;

    public Zug() {
        wegpunkte = new LinkedList<>();
    }

    private Zug(List<Position> wegpunkte) {
        this.wegpunkte = new LinkedList<>(wegpunkte);
    }

    public boolean istNeu() {
        return wegpunkte.isEmpty();
    }

    public Position startpunkt() {
        return wegpunkte.getFirst();
    }

    public Position endpunkt() {
        return wegpunkte.getLast();
    }

    public Zug erweitertUm(Position position) {
        if (hatWegpunkt(position)) {
            return teilzugBis(position);
        } else {
            List<Position> erweiterteWegpunkte = new LinkedList<>(wegpunkte);
            erweiterteWegpunkte.add(position);
            return new Zug(erweiterteWegpunkte);
        }
    }

    public int laenge() {
        return wegpunkte.size();
    }

    public Position wegpunktAnPosition(int index) {
        return wegpunkte.get(index);
    }

    public boolean hatWegpunkt(Position wegpunkt) {
        return wegpunkte.contains(wegpunkt);
    }

    public int positionVon(Position wegpunkt) {
        return wegpunkte.indexOf(wegpunkt);
    }

    public Zug teilzugBis(Position wegpunkt) {
        List<Position> gekuerzteWegpunkte = wegpunkte.subList(0, positionVon(wegpunkt));
        return new Zug(gekuerzteWegpunkte);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Zug zug = (Zug) o;
        return Objects.equals(wegpunkte, zug.wegpunkte);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wegpunkte);
    }

    public Zug rueckwaerts() {
        List<Position> reversePositions = new LinkedList<>(this.wegpunkte);
        Collections.reverse(reversePositions);
        return new Zug(reversePositions);
    }

    @Override
    public String toString() {
        return "Zug [" + wegpunkte + "]";
    }
}
