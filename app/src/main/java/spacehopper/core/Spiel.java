package spacehopper.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;

import static java.util.Collections.emptyList;

public class Spiel implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(Spiel.class);

    private final UUID spielId = UUID.randomUUID();
    private final Spielfeld spielfeld;
    private final List<Spieler> teilnehmer;

    private List<Zug> zuege;
    private int zugNummer;

    public Spiel(List<Spieler> teilnehmer, SpielfeldTyp spielfeldTyp) {
        this.teilnehmer = Collections.unmodifiableList(teilnehmer);
        if (SpielfeldTyp.STERN.equals(spielfeldTyp)) {
            spielfeld = new SpielfeldStern(this.teilnehmer);
        } else {
            spielfeld = new SpielfeldQuadrat(this.teilnehmer);
        }

        zuege = emptyList();
        zugNummer = 1;
    }

    public UUID id() {
        return spielId;
    }

    public Spielfeld spielfeld() {
        return spielfeld;
    }

    public List<Spieler> teilnehmer() {
        return teilnehmer;
    }

    public int aktiverSpielerIndex() {
        return (zugNummer() - 1) % anzahlSpieler();
    }

    private int anzahlSpieler() {
        return teilnehmer().size();
    }

    public int zugNummer() {
        return zugNummer;
    }

    public int anzahlZuege() {
        return zuege.size();
    }

    public Zug zug(int zugNummer) {
        return zuege.get(zugNummer - 1);
    }

    public void wendeAn(Zug zug) {
        if (anzahlZuege() >= zugNummer()) {
            LOG.warn("Setze Spielverlauf zurück auf Zug #{}", zugNummer());
            setzeZurueckAufZug(zugNummer());
        }
        ziehe(zug);

        List<Zug> tmpZuege = new LinkedList<>(zuege);
        tmpZuege.add(zug);
        zuege = List.copyOf(tmpZuege);
        zugNummer = anzahlZuege() + 1;
    }

    public void setzeZurueckAufZug(int zugNummer) {
        LinkedList<Zug> ruecknahmen =
                new LinkedList<>(zuege.subList(zugNummer, zuege.size()));
        ruecknahmen.descendingIterator()
                   .forEachRemaining(zug -> ziehe(zug.rueckwaerts()));
        zuege = zuege.subList(0, zugNummer);
    }

    public Spieler aktiverSpieler() {
        return teilnehmer().get(aktiverSpielerIndex());
    }

    public boolean zugZurueck() {
        if (zugNummer() > 1) {
            zugNummer--;
            Zug zug = zug(zugNummer());
            ziehe(zug.rueckwaerts());
            return true;
        }
        return false;
    }

    private void ziehe(Zug zug) {
        Brettpunkt ziel = spielfeld().brettpunktAn(zug.endpunkt());
        Optional<Spielfigur> figur = aktiverSpieler().figurAn(zug.startpunkt());
        figur.orElseThrow().zieheNach(ziel);
    }

    public Zug zuletztGespielterZug() {
        return zug(zugNummer());
    }

    public boolean zugVor() {
        if (anzahlZuege() >= zugNummer()) {
            zugNummer++;
            Zug zug = zuletztGespielterZug();
            ziehe(zug);
            return true;
        }
        return false;
    }

    public int runde() {
        return zugNummer() / anzahlSpieler();
    }
}
