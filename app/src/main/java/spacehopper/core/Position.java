package spacehopper.core;

import java.awt.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Volker
 */
public class Position implements Serializable {

    private final int x;
    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position(Point p) {
        x = p.x;
        y = p.y;
    }

    public static Position of(int x, int y) {
        return new Position(x, y);
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public Position oben() {
        return Position.of(x(), y() - 1);
    }

    public Position unten() {
        return Position.of(x(), y() + 1);
    }

    public Position links() {
        return Position.of(x() - 1, y());
    }

    public Position rechts() {
        return Position.of(x() + 1, y());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return String.format("[%d, %d]", x(), y());
    }
}
