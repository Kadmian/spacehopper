package spacehopper.core;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public class Haus {

    private final Set<Brettpunkt> brettpunkte;

    public Haus(Set<Brettpunkt> brettpunkte) {
        this.brettpunkte = brettpunkte;
    }

    public boolean istBesetztDurch(Spieler spieler) {
        Set<Position> spielerPosition = spieler.spielfiguren()
                                               .stream()
                                               .map(Spielfigur::position)
                                               .collect(toSet());

        return spielerPosition.equals(brettpunkte.stream()
                                                 .map(Brettpunkt::position)
                                                 .collect(Collectors.toSet()));
    }

    public Stream<Brettpunkt> brettpunkte() {
        return brettpunkte.stream();
    }
}
