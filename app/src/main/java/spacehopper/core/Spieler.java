package spacehopper.core;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Spieler implements Serializable {

    private final SpielerProfil profil;
    private final List<Spielfigur> spielfiguren = new LinkedList<>();

    private boolean cheater;

    public Spieler(SpielerProfil profil) {
        this.profil = profil;
        this.cheater = !SpielerTyp.HUMAN.equals(this.profil.typ());
    }

    public String getName() {
        return profil.name();
    }

    public SpielerTyp typ() {
        return profil.typ();
    }

    public boolean getCheater() {
        return cheater;
    }

    public void setCheater(boolean cheater) {
        this.cheater = cheater;
    }

    public List<Spielfigur> spielfiguren() {
        return spielfiguren;
    }

    public void add(Spielfigur spielfigur) {
        spielfiguren.add(spielfigur);
    }

    public void stelleFigurenAuf(Haus startHaus) {
        startHaus.brettpunkte()
                 .forEach(brettpunkt -> add(new Spielfigur(this, brettpunkt)));

    }

    public Optional<Spielfigur> figurAn(Position position) {
        return spielfiguren().stream()
                             .filter(figur -> position.equals(figur.position()))
                             .findFirst();
    }
}
