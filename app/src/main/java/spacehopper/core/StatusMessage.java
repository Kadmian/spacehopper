package spacehopper.core;

public enum StatusMessage {
    FIGURGEWAEHLT(1),
    ILLEGALFIGUR(2),
    ILLEGALFELD(3),
    FIGURZURUECK(4),
    ZUGAUSFUEHREN(5),
    ZUGZURUECK(6),
    ILLEGALZUGZURUECK(7),
    TEILZUGZURUECK(8),
    ZUGERWEITERN(9),
    SPIELSTATUS(10),
    SPIELBEENDET(11),
    ZUGWEITER(12);

    private final int legacyMessageId;

    StatusMessage(int legacyMessageId) {
        this.legacyMessageId = legacyMessageId;
    }

    public int legacyMessageId() {
        return legacyMessageId;
    }
}
