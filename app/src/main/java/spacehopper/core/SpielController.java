package spacehopper.core;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public class SpielController implements Serializable {

    private final Spiel spiel;

    private boolean gewonnen;

    private boolean geschoben;
    private Zug aktiverZug;

    public SpielController(List<Spieler> teilnehmer, SpielfeldTyp spielfeldTyp) {
        this.spiel = new Spiel(teilnehmer, spielfeldTyp);

        gewonnen = false;

        aktiverZug = new Zug();
    }

    /**
     * Liefert ein GuiUpdate mit den durch den Klick verursachten Veraenderungen.
     *
     * @param position Geclickte Position
     * @return guiUpdate
     * @author patrick, carsten
     */
    public SpielStatus klickeFeld(Position position) {
        SpielStatus spielStatus = new SpielStatus(spiel.id());
        spielStatus.setAktiverSpielerIndex(spiel.aktiverSpielerIndex());
        spielStatus.setSpielfeldTyp(spiel.spielfeld().typ());
        spielStatus.setRunde(aktuelleRunde());
        spielStatus.setSpielerListe(spiel.teilnehmer());
        spielStatus.setNextSpielerTyp(aktuellerSpieler().typ());
        if (gewonnen) {
            spielStatus.setGewonnen(false);
            spielStatus.setStatusMessage(StatusMessage.SPIELBEENDET);
            return spielStatus;
        }

        StatusMessage message;
        if (aktiverZug.istNeu()) {
            if (aktuellerSpieler().figurAn(position).isPresent()) {
                // gueltiger Zug
                aktiverZug = aktiverZug.erweitertUm(position);
                //Point, Figur     ,markiert
                spielStatus.setPfad(new int[]{position.x(), position.y(),
                                              spiel.aktiverSpielerIndex(), 1});
                message = StatusMessage.FIGURGEWAEHLT;
            } else {
                // ungueltiger Zug
                message = StatusMessage.ILLEGALFIGUR;
            }
        } else {
            // Zug fortsetzen
            if (position.equals(aktiverZug.endpunkt())) {
                // doppelClick
                if (schiebenIstMoeglich()) {
                    // andere Figur waehlen
                    setzeAktivenZugZurueck();
                    spielStatus.setPfad(new int[]{position.x(), position.y(),
                                                  spiel.aktiverSpielerIndex(), 0});
                    message = StatusMessage.FIGURZURUECK;
                }
                // Zug beenden
                else {
                    // Pfad demarkieren, Figur umsetzen
                    int[] pfad = new int[aktiverZug.laenge() * 4];
                    for (int i = 0; i < aktiverZug.laenge(); i++) {
                        pfad[i * 4] = aktiverZug.wegpunktAnPosition(i).x();
                        pfad[i * 4 + 1] = aktiverZug.wegpunktAnPosition(i).y();
                        pfad[i * 4 + 2] = -1;
                        pfad[i * 4 + 3] = 0;
                    }
                    Spieler ziehenderSpieler = spiel.aktiverSpieler();
                    pfad[aktiverZug.laenge() * 4 - 2] = spiel.aktiverSpielerIndex();
                    spielStatus.setPfad(pfad);
                    // neuen Zug speichern
                    if (spiel.anzahlZuege() >= spiel.zugNummer()) {
                        ziehenderSpieler.setCheater(true);
                    }
                    spiel.wendeAn(aktiverZug);

                    resetAktiverZug();
                    if (hatGewonnen(ziehenderSpieler)) {
                        gewonnen = true;
                        spielStatus.setGewonnen(true);
                        spielStatus.setCheater(aktuellerSpieler().getCheater());
                        message = StatusMessage.SPIELBEENDET;
                    } else {
                        spielStatus.setAktiverSpielerIndex(spiel.aktiverSpielerIndex());
                        spielStatus.setRunde(spiel.zugNummer() / anzahlSpieler());
                        spielStatus.setNextSpielerTyp(aktuellerSpieler().typ());
                        message = StatusMessage.ZUGAUSFUEHREN;
                    }
                }
            } else if (aktiverZug.hatWegpunkt(position)) {
                // teilZug zuruecknehmen
                int[] pfad = new int[(aktiverZug.laenge()
                                      - 1
                                      - aktiverZug.positionVon(position)) * 4];
                for (int i = aktiverZug.laenge() - 1, k = 0;
                     i > aktiverZug.positionVon(position);
                     i--, k++) {
                    pfad[k * 4] = aktiverZug.wegpunktAnPosition(i).x();
                    pfad[k * 4 + 1] = aktiverZug.wegpunktAnPosition(i).y();
                    pfad[k * 4 + 2] = -1;
                    pfad[k * 4 + 3] = 0;
                }
                spielStatus.setPfad(pfad);
                aktiverZug = aktiverZug.erweitertUm(position);
                geschoben = false;
                message = StatusMessage.TEILZUGZURUECK;
            } else if (erreichbar(position)) {
                // Zug erweitern
                aktiverZug = aktiverZug.erweitertUm(position);
                spielStatus.setPfad(new int[]{position.x(), position.y(), -1, 1});
                message = StatusMessage.ZUGERWEITERN;
            } else {
                message = StatusMessage.ILLEGALFELD;
            }
        }
        return spielStatus.withMessage(message);
    }

    private void resetAktiverZug() {
        aktiverZug = new Zug();
        geschoben = false;
    }

    private int aktuelleRunde() {
        return spiel.runde();
    }

    private int anzahlSpieler() {
        return spiel.teilnehmer().size();
    }

    private boolean hatGewonnen(Spieler spieler) {
        Haus zielHaus = spielfeld().zielHausFuer(spieler);
        return zielHaus.istBesetztDurch(spieler);
    }

    private Spielfeld spielfeld() {
        return spiel.spielfeld();
    }

    private Spieler aktuellerSpieler() {
        return spiel.aktiverSpieler();
    }

    private Optional<Spielfigur> figurAn(Position position) {
        return aktuellerSpieler().figurAn(position);
    }

    /**
     * Prueft, ob der uebergebene Punkt erreichbar ist
     *
     * @param position x- und y-Position des zu pruefenden Brettpunktes
     * @return gibt an, ob der Punkt erreichbar ist
     * @author patrick, carsten
     */
    private boolean erreichbar(Position position) {
        if (geschoben || istBesetzt(position)) {
            return false;
        }
        for (int richtung = 0; richtung < 8; richtung++) {
            Optional<Brettpunkt> nachbar =
                    spielfeld().brettpunktAn(aktiverZug.endpunkt()).getNachbarn(richtung);
            if (nachbar.isEmpty()) {
                continue;
            }
            if (istSchieben(position, richtung, nachbar.get()) || istSpringen(position,
                                                                              richtung,
                                                                              nachbar.get())) {
                return true;
            }
        }
        return false;
    }

    private boolean istBesetzt(Position p) {
        return figurAn(p).isPresent();
    }

    private boolean istSchieben(Position p, int richtung, Brettpunkt nachbar) {
        if (schiebenIstMoeglich()
            && nachbar == spielfeld().brettpunktAn(p)
            && nachbar.istBesetzbarDurch(aktuellerSpieler())
            && (SpielfeldTyp.STERN.equals(spielfeld().typ()) || richtung % 2 == 1)) {
            geschoben = true;
            return true;
        }
        return false;
    }

    private boolean istSpringen(Position p, int richtung, Brettpunkt nachbar) {
        if (!geschoben && nachbar.istBesetzt()) {
            Optional<Brettpunkt> nachbarOpt = nachbar.getNachbarn(richtung);
            return nachbarOpt.isPresent()
                   && (nachbarOpt.get() == brettpunktAn(p))
                   && nachbarOpt.get().istBesetzbarDurch(aktuellerSpieler());
        }
        return false;
    }

    private Brettpunkt brettpunktAn(Position p) {
        return spielfeld().brettpunktAn(p);
    }

    private boolean schiebenIstMoeglich() {
        return aktiverZug.laenge() == 1;
    }

    /**
     * Die Methode nimmt den letzten Zug zurueck, wenn einer existiert. Wenn gerade ein
     * anderer Zug aufgebaut wird, wird die Statusmeldung 200 an die GUI gegeben
     *
     * @return Die Aenderungen, die die GUI anzeigen soll.
     * @author patrick, carsten
     */
    public SpielStatus zugZuruecknehmen() {
        SpielStatus spielStatus = new SpielStatus(spiel.id());
        if (!aktiverZug.istNeu()) {
            spielStatus.setStatusMessage(StatusMessage.ILLEGALZUGZURUECK);
        } else {
            Zug tmp = spiel.zuletztGespielterZug();
            if (spiel.zugZurueck()) {
                spielStatus.setPfad(new int[]{tmp.endpunkt().x(), tmp.endpunkt().y(), -1,
                                              0, tmp.startpunkt().x(),
                                              tmp.startpunkt().y(),
                                              spiel.aktiverSpielerIndex(), 0});
                spielStatus.setStatusMessage(StatusMessage.ZUGZURUECK);
            }
        }
        spielStatus.setAktiverSpielerIndex(spiel.aktiverSpielerIndex());
        spielStatus.setSpielerListe(spiel.teilnehmer());
        spielStatus.setSpielfeldTyp(spielfeld().typ());
        spielStatus.setRunde(aktuelleRunde());
        spielStatus.setNextSpielerTyp(aktuellerSpieler().typ());
        return spielStatus;
    }

    public SpielStatus zugWeitergehen() {
        SpielStatus spielStatus = new SpielStatus(spiel.id());
        int letzterZugSpielerIndex = spiel.aktiverSpielerIndex();
        if (spiel.zugVor()) {
            Zug zug = spiel.zuletztGespielterZug();
            spielStatus.setPfad(new int[]{zug.startpunkt().x(), zug.startpunkt().y(), -1,
                                          0, zug.endpunkt().x(), zug.endpunkt().y(),
                                          letzterZugSpielerIndex, 0});
            spielStatus.setStatusMessage(StatusMessage.ZUGWEITER);
        }
        spielStatus.setAktiverSpielerIndex(spiel.aktiverSpielerIndex());
        spielStatus.setSpielfeldTyp(spielfeld().typ());
        spielStatus.setRunde(aktuelleRunde());
        spielStatus.setNextSpielerTyp(aktuellerSpieler().typ());
        return spielStatus;
    }

    /**
     * lifert den aktuellen Spielstatus in einem GuiUpdate zurueck
     *
     * @return der aktuelle Spielstatus
     * @author patrick, carsten
     */
    public SpielStatus getSpielStatus() {
        SpielStatus spielStatus = new SpielStatus(spiel.id());
        spielStatus.setAktiverSpielerIndex(spiel.aktiverSpielerIndex());
        spielStatus.setSpielfeldTyp(spielfeld().typ());
        spielStatus.setRunde(spiel.anzahlZuege() / anzahlSpieler() + 1);
        List<Brettpunkt> brettpunkte = spielfeld().brettpunkte();
        int anzahl = brettpunkte.size() + aktiverZug.laenge();
        spielStatus.setSpielerListe(spiel.teilnehmer());
        int[] pfad = new int[anzahl * 4];
        for (int i = 0; i < brettpunkte.size(); i++) {
            Brettpunkt brettpunkt = brettpunkte.get(i);
            pfad[i * 4] = brettpunkt.position().x();
            pfad[i * 4 + 1] = brettpunkt.position().y();
            pfad[i * 4 + 2] = (brettpunkt.spielfigur() == null)
                              ? -1
                              : getSpielerIndex(brettpunkt.spielfigur().getSpieler());
            pfad[i * 4 + 3] = 0;
        }
        int k = brettpunkte.size();
        if (!aktiverZug.istNeu()) {
            pfad[k * 4] = aktiverZug.startpunkt().x();
            pfad[k * 4 + 1] = aktiverZug.startpunkt().y();
            Spieler spieler = figurAn(aktiverZug.startpunkt()).map(Spielfigur::getSpieler)
                                                              .orElse(null);
            pfad[k * 4 + 2] = getSpielerIndex(spieler);
            pfad[k * 4 + 3] = 1;
            k++;
            for (int i = 1; i < aktiverZug.laenge(); i++) {
                pfad[k * 4] = aktiverZug.wegpunktAnPosition(i).x();
                pfad[k * 4 + 1] = aktiverZug.wegpunktAnPosition(i).y();
                pfad[k * 4 + 2] = -1;
                pfad[k * 4 + 3] = 1;
                k++;
            }
        }
        spielStatus.setPfad(pfad);
        spielStatus.setStatusMessage(StatusMessage.SPIELSTATUS);
        if (gewonnen) {
            spielStatus.setStatusMessage(StatusMessage.SPIELBEENDET);
        }
        spielStatus.setNextSpielerTyp(aktuellerSpieler().typ());
        return spielStatus;
    }

    private int getSpielerIndex(Spieler spieler) {
        for (int spielerIndex = 0;
             spielerIndex < spiel.teilnehmer().size();
             spielerIndex++) {
            if (spiel.teilnehmer().get(spielerIndex) == spieler) {
                return spielerIndex;
            }
        }
        return -1;
    }

    public Spielfeld getSpielfeld() {
        return spielfeld();
    }

    public Spieler[] getTeilnehmer() {
        return spiel.teilnehmer().toArray(new Spieler[0]);
    }

    public int getAktiverSpielerIndex() {
        return spiel.aktiverSpielerIndex();
    }

    public int getSpielfeldTyp() {
        return spielfeld().typ().legacyType();
    }

    public void setzeAktivenZugZurueck() {
        aktiverZug = new Zug();
    }

}
