package spacehopper.core;

import spacehopper.gui.PfadElement;
import spacehopper.ki.KIRunner;
import spacehopper.netzwerk.ComObject;
import spacehopper.netzwerk.NetzwerkClient;
import spacehopper.netzwerk.NetzwerkServer;
import spacehopper.savegames.GameStore;

import java.util.*;
import java.util.stream.IntStream;

public class Spielverwaltung {

    private final ComObject comObject;
    private final List<Listener> listeners = new LinkedList<>();
    private SpielController currentGame;
    private SpielUpdateListener spielUpdateListener;
    private SpielerTyp nextSpielerTyp;
    private NetzwerkClient netzwerkClient;
    private NetzwerkServer netzwerkServer;

    public Spielverwaltung() {
        comObject = new ComObject();
    }

    public void zugVorschlagen() {
        currentGame.getTeilnehmer()[currentGame.getAktiverSpielerIndex()].setCheater(true);
        if (SpielerTyp.HUMAN.equals(nextSpielerTyp)) {
            try {
                KIRunner kiRunner = new KIRunner(currentGame, this);
                nextSpielerTyp = SpielerTyp.COMPUTER_MEDIUM;
                kiRunner.schlageZugVor();
            } catch (Exception ignored) {
            }
        }
    }

    public void startKI() {
        try {
            KIRunner kiRunner = new KIRunner(currentGame, this);
            kiRunner.fuehreZugAus();
        } catch (Exception ignored) {
        }
    }

    public void zugZuruecknehmen() {
        SpielStatus spielStatus = currentGame.zugZuruecknehmen();
        if (spielStatus.getStatusMessage() == StatusMessage.ZUGZURUECK) {
            nextSpielerTyp = SpielerTyp.HUMAN;
        }
        spielUpdateListener.werteUpdateAus(spielStatus);
    }

    public void zugWeitergehen() {
        SpielStatus spielStatus = currentGame.zugWeitergehen();
        if (spielStatus.getStatusMessage() == StatusMessage.ZUGWEITER) {
            nextSpielerTyp = SpielerTyp.HUMAN;
        }
        spielUpdateListener.werteUpdateAus(spielStatus);
    }

    public void newGame(List<Spieler> teilnehmer, SpielfeldTyp spielfeldTyp) {
        beendeMoeglichesNetzwerkspiel();
        SpielStatus spielStatus;
        if (isNetworkgame(teilnehmer)) {
            List<Spieler> netzwerkSpielTeilnehmer =
                    initializeNetworkGame(teilnehmer, spielfeldTyp);
            spielStatus = createGame(netzwerkSpielTeilnehmer, spielfeldTyp);
        } else {
            spielStatus = createGame(teilnehmer, spielfeldTyp);
        }
        startGame(spielStatus);
    }

    private void beendeMoeglichesNetzwerkspiel() {
        if (netzwerkServer != null) {
            netzwerkServer.beenden();
            netzwerkServer = null;
        }
        if (netzwerkClient != null) {
            netzwerkClient.beenden();
            netzwerkClient = null;
        }
    }

    private boolean isNetworkgame(List<Spieler> teilnehmer) {
        return teilnehmer.stream()
                         .map(Spieler::typ)
                         .anyMatch(SpielerTyp.NETWORK_PLAYER::equals);
    }

    private List<Spieler> initializeNetworkGame(List<Spieler> teilnehmer,
                                                SpielfeldTyp spielfeldTyp) {
        String[] spielerNamen =
                teilnehmer.stream().map(Spieler::getName).toArray(String[]::new);
        int[] legacySpielerTypen = teilnehmer.stream()
                                             .map(Spieler::typ)
                                             .mapToInt(SpielerTyp::legacyType)
                                             .toArray();
        startNetworkServer(spielfeldTyp, spielerNamen, legacySpielerTypen);

        return teilnehmer.stream()
                         .map(spieler -> new SpielerProfil(spieler.getName(),
                                                           SpielerTyp.NETWORK_PLAYER))
                         .map(Spieler::new)
                         .toList();
    }

    private void startNetworkServer(SpielfeldTyp spielfeldTyp,
                                    String[] spielerNamen,
                                    int[] legacySpielerTypen) {
        netzwerkServer = new NetzwerkServer(this,
                                            comObject,
                                            spielfeldTyp.legacyType(),
                                            spielerNamen,
                                            legacySpielerTypen);
        netzwerkServer.start();
    }

    private SpielStatus createGame(List<Spieler> teilnehmer, SpielfeldTyp spielfeldTyp) {
        currentGame = new SpielController(teilnehmer, spielfeldTyp);
        SpielStatus spielStatus = currentGame.getSpielStatus();
        nextSpielerTyp = spielStatus.getNextSpielerTyp();
        return spielStatus;
    }

    private void startGame(SpielStatus update) {
        if (isComputerPlayer(nextSpielerTyp)) {
            startKI();
        }
        spielUpdateListener.werteUpdateAus(update);
    }

    public void newGameStart(int[] spielerTypen,
                             String[] spielernamen,
                             SpielfeldTyp spielfeldTyp) {
        List<Spieler> teilnehmer = IntStream.range(0, spielerTypen.length)
                                            .mapToObj(i -> new SpielerProfil(spielernamen[i],
                                                                             SpielerTyp.byLegacyType(
                                                                                               spielerTypen[i])
                                                                                       .orElse(SpielerTyp.HUMAN)))
                                            .map(Spieler::new)
                                            .toList();
        SpielStatus update = createGame(teilnehmer, spielfeldTyp);
        startGame(update);
    }

    public void newNetworkgame(String spielername, int spielertyp, String ip) {
        netzwerkClient = new NetzwerkClient(this, comObject, spielername, spielertyp, ip);
        netzwerkClient.start();
    }

    public synchronized void klickeFeld(SpielerTyp spielerTyp, Position position) {
        if (spielerTyp == nextSpielerTyp) {
            SpielStatus update = currentGame.klickeFeld(position);

            if (update.getGewonnen()) {
                meldeSpielende(update);
            } else if (update.getStatusMessage() == StatusMessage.ZUGAUSFUEHREN) {
                nextSpielerTyp = update.getNextSpielerTyp();
            }

            if ((update.getStatusMessage() == StatusMessage.ZUGAUSFUEHREN
                 || update.getGewonnen()) && spielerTyp != SpielerTyp.NETWORK_PLAYER) {
                Pfad pfad = update.getPfad();
                Zug zug = new Zug();
                for (PfadElement pfadElement : pfad) {
                    zug = zug.erweitertUm(new Position(pfadElement.punkt()));
                }
                comObject.setZug(zug);
            }

            if (isComputerPlayer(nextSpielerTyp)
                && update.getStatusMessage() == StatusMessage.ZUGAUSFUEHREN) {
                startKI();
            }
            spielUpdateListener.werteUpdateAus(update);
        }
    }

    private boolean isComputerPlayer(SpielerTyp nextSpielerTyp) {
        return nextSpielerTyp != SpielerTyp.HUMAN
               && this.nextSpielerTyp != SpielerTyp.NETWORK_PLAYER;
    }

    private void meldeSpielende(SpielStatus update) {
        listeners.forEach(listener -> listener.spielBeendet(update));
    }

    public void register(SpielUpdateListener spielUpdateListener) {
        this.spielUpdateListener = spielUpdateListener;
    }

    public void gibKontrolleZurueck() {
        nextSpielerTyp = SpielerTyp.HUMAN;
    }

    public Vector<Position> suchePfad(Vector<Brettpunkt> bekannt,
                                      Brettpunkt ausgangspunkt,
                                      Brettpunkt zielpunkt) {
        if (ausgangspunkt.position().equals(zielpunkt.position())) {
            return new Vector<>(List.of(zielpunkt.position()));
        }
        bekannt.addElement(ausgangspunkt);
        Brettpunkt current;
        for (int i = 0; i < 8; i++) {
            Optional<Brettpunkt> nachbarOpt = ausgangspunkt.getNachbarn(i);
            if (nachbarOpt.isPresent() && nachbarOpt.get().istBesetzt()) {
                Optional<Brettpunkt> sprungziel = nachbarOpt.get().getNachbarn(i);
                if (sprungziel.isPresent()
                    && besetzbar(sprungziel.get())
                    && !bekannt.contains(sprungziel.get())) {
                    Vector<Position> pruefe =
                            suchePfad(bekannt, sprungziel.get(), zielpunkt);
                    if (pruefe != null) {
                        pruefe.addElement(ausgangspunkt.position());
                        return pruefe;
                    }
                }
            }
        }
        return null;
    }

    private boolean besetzbar(Brettpunkt brettpunkt) {
        Spieler aktiverSpieler =
                currentGame.getTeilnehmer()[currentGame.getAktiverSpielerIndex()];
        return brettpunkt.istBesetzbarDurch(aktiverSpieler);
    }

    public Vector<Position> erreichbar(Zug zug) {
        Spielfeld spielfeld = currentGame.getSpielfeld();
        for (int richtung = 0; richtung < 8; richtung++) {
            Optional<Brettpunkt> nachbarOpt = currentGame.getSpielfeld()
                                                         .brettpunktAn(zug.startpunkt())
                                                         .getNachbarn(richtung);
            if (nachbarOpt.isPresent()
                && nachbarOpt.get() == currentGame.getSpielfeld()
                                                  .brettpunktAn(zug.endpunkt())
                && besetzbar(nachbarOpt.get())) {
                Vector<Position> rueckgabe = new Vector<>();
                rueckgabe.add(zug.startpunkt());
                rueckgabe.add(zug.endpunkt());
                return rueckgabe;
            }
        }

        Vector<Position> reverse = suchePfad(new Vector<>(),
                                             spielfeld.brettpunktAn(zug.startpunkt()),
                                             spielfeld.brettpunktAn(zug.endpunkt()));
        if (reverse != null) {
            Vector<Position> rueckgabe = new Vector<>(reverse);
            Collections.reverse(rueckgabe);
            return rueckgabe;
        }
        return null;
    }

    public SpielStatus loadAutosaveGame() {
        currentGame = GameStore.loadAutosaveGame();
        return reinitialize();
    }

    private SpielStatus reinitialize() {
        SpielStatus update = currentGame.getSpielStatus();
        nextSpielerTyp = update.getNextSpielerTyp();
        if (!SpielerTyp.HUMAN.equals(nextSpielerTyp)
            && !SpielerTyp.NETWORK_PLAYER.equals(nextSpielerTyp)
            && !StatusMessage.SPIELBEENDET.equals(update.getStatusMessage())) {
            startKI();
        }
        return update;
    }

    public SpielStatus loadGame(int slot) {
        currentGame = GameStore.loadGame(slot);
        return reinitialize();
    }

    public void autosaveGame() {
        if (nextSpielerTyp != SpielerTyp.HUMAN) {
            currentGame.setzeAktivenZugZurueck();
        }
        GameStore.autosave(currentGame);
    }

    public void register(Listener listener) {
        listeners.add(listener);
    }

    public SpielController currentGame() {
        return currentGame;
    }

    public interface Listener {

        void spielBeendet(SpielStatus update);

    }

}
