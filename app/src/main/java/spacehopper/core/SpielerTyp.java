package spacehopper.core;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author Volker
 */
public enum SpielerTyp {

    HUMAN(0),
    COMPUTER_EASY(1),
    COMPUTER_MEDIUM(2),
    COMPUTER_HARD(3),
    NETWORK_PLAYER(4);

    private final int legacyType;

    SpielerTyp(int legacyType) {
        this.legacyType = legacyType;
    }

    public static Optional<SpielerTyp> byLegacyType(int legacyTypeToFind) {
        return Arrays.stream(values())
                     .filter(spielerTyp -> spielerTyp.legacyType() == legacyTypeToFind)
                     .findAny();
    }

    public int legacyType() {
        return legacyType;
    }

}
