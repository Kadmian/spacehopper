package spacehopper.core;

import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

public class SpielStatus {

    private final UUID spielId;
    private Pfad pfad;
    private int runde;
    private int aktiverSpielerIndex;
    private boolean gewonnen;
    private boolean cheater;
    private StatusMessage statusMessage;
    private boolean highscore;
    private SpielfeldTyp spielfeldTyp;
    private List<Spieler> spielerListe;
    private SpielerTyp nextSpielerTyp;

    public SpielStatus(UUID spielId) {
        this.spielId = spielId;
    }

    public Pfad getPfad() {
        return pfad;
    }

    public void setPfad(int[] pfad) {
        this.pfad = new Pfad(pfad);
    }

    public int getRunde() {
        return runde;
    }

    public void setRunde(int runde) {
        this.runde = runde;
    }

    public int getAktiverSpielerIndex() {
        return aktiverSpielerIndex;
    }

    public void setAktiverSpielerIndex(int aktiverSpielerIndex) {
        this.aktiverSpielerIndex = aktiverSpielerIndex;
    }

    public boolean getGewonnen() {
        return gewonnen;
    }

    public void setGewonnen(boolean gewonnen) {
        this.gewonnen = gewonnen;
    }

    public boolean getCheater() {
        return cheater;
    }

    public void setCheater(boolean cheater) {
        this.cheater = cheater;
    }

    public StatusMessage getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(StatusMessage statusMessage) {
        this.statusMessage = statusMessage;
    }

    public SpielStatus withMessage(StatusMessage message) {
        setStatusMessage(message);
        return this;
    }

    public boolean getHighscore() {
        return highscore;
    }

    public void setHighscore(boolean highscore) {
        this.highscore = highscore;
    }

    public SpielfeldTyp spielfeldTyp() {
        return spielfeldTyp;
    }

    public void setSpielfeldTyp(SpielfeldTyp spielfeldTyp) {
        this.spielfeldTyp = spielfeldTyp;
    }

    public void setSpielerListe(List<Spieler> spielerListe) {
        this.spielerListe = spielerListe;
    }

    public String[] getSpielerNamen() {
        return spielerListe.stream().map(Spieler::getName).toArray(String[]::new);
    }

    public SpielerTyp getNextSpielerTyp() {
        return nextSpielerTyp;
    }

    public void setNextSpielerTyp(SpielerTyp nextSpielerTyp) {
        this.nextSpielerTyp = nextSpielerTyp;
    }

    public int[] getSpielerTypen() {
        int[] spielerTypen = new int[spielerListe.size()];
        IntStream.range(0, spielerListe.size())
                 .forEach(i -> spielerTypen[i] = spielerListe.get(i).typ().legacyType());
        return spielerTypen;
    }

    public List<Spieler> teilnehmer() {
        return spielerListe;
    }

    public UUID getSpielId() {
        return spielId;
    }
}
