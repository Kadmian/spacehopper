package spacehopper.core;

import java.io.Serializable;

public class SpielerProfil implements Serializable {

    private final String name;
    private final SpielerTyp typ;

    public SpielerProfil(String name, SpielerTyp typ) {
        this.name = name;
        this.typ = typ;
    }

    public String name() {
        return name;
    }

    public SpielerTyp typ() {
        return typ;
    }
}
