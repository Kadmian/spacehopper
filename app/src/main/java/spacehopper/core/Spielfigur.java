package spacehopper.core;

public class Spielfigur implements java.io.Serializable {

    private final Spieler spieler;
    private Brettpunkt brettpunkt;

    public Spielfigur(Spieler spieler, Brettpunkt brettpunkt) {
        this.spieler = spieler;
        this.brettpunkt = brettpunkt;
        brettpunkt.setSpielfigur(this);
    }

    public Position position() {
        return brettpunkt.position();
    }

    public void zieheNach(Brettpunkt brettpunkt) {
        this.brettpunkt.setSpielfigur(null);
        brettpunkt.setSpielfigur(this);
        this.brettpunkt = brettpunkt;
    }

    public Spieler getSpieler() {
        return spieler;
    }

}
