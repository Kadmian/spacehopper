package spacehopper.core;

import java.util.List;

public class SpielfeldQuadrat extends Spielfeld {

    public SpielfeldQuadrat(List<Spieler> spieler) {
        Spieler startFuer;
        Spieler zielFuer;

        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                Position position = Position.of(x, y);
                startFuer = null;
                zielFuer = null;

                if (spieler.size() == 1) {
                    if (x + y < 6 && !(x == 0 && y == 5) && !(x == 5
                                                              && y == 0)) {
                        startFuer = spieler.get(0);
                    }
                    if (x + y > 24 && !(x == 15 && y == 10) && !(x == 10
                                                                 && y == 15)) {
                        zielFuer = spieler.get(0);
                    }
                }
                if (spieler.size() == 2) {
                    if (x + y < 6 && !(x == 0 && y == 5) && !(x == 5
                                                              && y == 0)) {
                        startFuer = spieler.get(0);
                        zielFuer = spieler.get(1);
                    }
                    if (x + y > 24 && !(x == 15 && y == 10) && !(x == 10
                                                                 && y == 15)) {
                        startFuer = spieler.get(1);
                        zielFuer = spieler.get(0);
                    }
                }
                if (spieler.size() == 4) {
                    if (isInUpperLeftHouse(x, y)) {
                        startFuer = spieler.get(0);
                        zielFuer = spieler.get(2);
                    }
                    if (isInUpperRightHouse(x, y)) {
                        startFuer = spieler.get(1);
                        zielFuer = spieler.get(3);
                    }
                    if (isInLowerLeftHouse(x, y)) {
                        startFuer = spieler.get(3);
                        zielFuer = spieler.get(1);
                    }
                    if (isInLowerRightHouse(x, y)) {
                        startFuer = spieler.get(2);
                        zielFuer = spieler.get(0);
                    }
                }

                add(new Brettpunkt(startFuer, zielFuer, false, position));
            }
        }

        verbindeBrettpunkte();
        spieler.forEach(s -> {
            s.stelleFigurenAuf(startHausFuer(s));
        });
    }

    private void verbindeBrettpunkte() {
        brettpunkte().forEach(brettpunkt -> {
            Position position = brettpunkt.position();
            brettpunkt.setNachbarn(OBEN, brettpunktAn(position.oben()));
            brettpunkt.setNachbarn(RECHTS_OBEN,
                                   brettpunktAn(position.rechts().oben()));
            brettpunkt.setNachbarn(RECHTS, brettpunktAn(position.rechts()));
            brettpunkt.setNachbarn(RECHTS_UNTEN,
                                   brettpunktAn(position.rechts().unten()));
            brettpunkt.setNachbarn(UNTEN, brettpunktAn(position.unten()));
            brettpunkt.setNachbarn(LINKS_UNTEN,
                                   brettpunktAn(position.links().unten()));
            brettpunkt.setNachbarn(LINKS, brettpunktAn(position.links()));
            brettpunkt.setNachbarn(LINKS_OBEN,
                                   brettpunktAn(position.links().oben()));
        });
    }

    private boolean isInUpperLeftHouse(int x, int y) {
        return x + y < 5 && !(x == 0 && y == 4) && !(x == 4 && y == 0);
    }

    private boolean isInUpperRightHouse(int x, int y) {
        return 15 - x + y < 5 && !(x == 11 && y == 0) && !(x == 15 && y == 4);
    }

    private boolean isInLowerLeftHouse(int x, int y) {
        return 15 - x + y > 25 && !(x == 0 && y == 11) && !(x == 4 && y == 15);
    }

    private boolean isInLowerRightHouse(int x, int y) {
        return x + y > 25 && !(x == 15 && y == 11) && !(x == 11 && y == 15);
    }

    @Override
    public SpielfeldTyp typ() {
        return SpielfeldTyp.QUADRAT;
    }

}
