package spacehopper.core;

import spacehopper.gui.PfadElement;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

/**
 * @author Volker
 */
public class Pfad implements Iterable<PfadElement> {

    private final List<PfadElement> pfad;

    public Pfad(int[] pfad) {
        this.pfad = IntStream.range(0, pfad.length / 4)
                             .mapToObj(i -> parseElement(pfad, i))
                             .collect(toList());
    }

    private static PfadElement parseElement(int[] pfad, int index) {
        return new PfadElement(pfad[index * 4],
                               pfad[index * 4 + 1],
                               pfad[index * 4 + 2],
                               pfad[index * 4 + 3]);
    }

    public int laenge() {
        return pfad.size();
    }

    public PfadElement get(int index) {
        return pfad.get(index);
    }

    public PfadElement anfang() {
        return get(0);
    }

    public PfadElement ende() {
        return get(laenge() - 1);
    }

    @Override
    public Iterator<PfadElement> iterator() {
        return pfad.iterator();
    }

    @Override
    public void forEach(Consumer<? super PfadElement> action) {
        pfad.forEach(action);
    }

    @Override
    public Spliterator<PfadElement> spliterator() {
        return pfad.spliterator();
    }
}
