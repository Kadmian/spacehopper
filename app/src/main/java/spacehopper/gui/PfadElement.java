package spacehopper.gui;

import java.awt.*;

/**
 * @author Volker
 */
public class PfadElement {

    private final int x;
    private final int y;
    private final int spielerIndex;
    private final int knotenStatus;

    public PfadElement(int x, int y, int spielerIndex, int knotenStatus) {
        this.x = x;
        this.y = y;
        this.spielerIndex = spielerIndex;
        this.knotenStatus = knotenStatus;
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public Point punkt() {
        return new Point(x, y);
    }

    public int spielerIndex() {
        return spielerIndex;
    }

    public int knotenStatus() {
        return knotenStatus;
    }

}
