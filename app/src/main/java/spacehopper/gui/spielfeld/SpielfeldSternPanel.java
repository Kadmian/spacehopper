package spacehopper.gui.spielfeld;

import spacehopper.core.Position;
import spacehopper.core.Spielverwaltung;
import spacehopper.image.ImageResource;

import java.util.HashMap;
import java.util.Map;

public class SpielfeldSternPanel extends SpielfeldPanel {

    public SpielfeldSternPanel(Spielverwaltung spielverwaltung) {
        super(spielverwaltung, ImageResource.BOARD_STAR);
    }

    protected Map<Position, GuiKnoten> createSpielfeld(Spielverwaltung spielverwaltung) {
        Map<Position, GuiKnoten> feld = new HashMap<>();
        int[] numberOfNodesInRow = { 1,
                                     2,
                                     3,
                                     4,
                                     13,
                                     12,
                                     11,
                                     10,
                                     9,
                                     10,
                                     11,
                                     12,
                                     13,
                                     4,
                                     3,
                                     2,
                                     1 };
        double CENTER = 305;
        double X_INSETS = 45;
        double Y_INSETS = 39;
        double Y_MARGIN = 0;
        for (int rowIndex = 0; rowIndex < 17; rowIndex++) {
            double f = numberOfNodesInRow[rowIndex] / 2.0;
            for (int columnIndex = 0;
                 columnIndex < numberOfNodesInRow[rowIndex];
                 columnIndex++) {
                GuiKnoten knoten = new GuiKnoten();
                Position position = new Position(rowIndex, columnIndex);
                knoten.addMouseListener(new NodeClickedListener(spielverwaltung, position));
                feld.put(position, knoten);
                // mitte bei 257-14
                knoten.locateAt(CENTER + X_INSETS / 2 + (columnIndex - f) * X_INSETS,
                                rowIndex * Y_INSETS + Y_MARGIN);
            }
        }
        return feld;
    }
}
