package spacehopper.gui.spielfeld;

import spacehopper.image.ImageResource;

import javax.swing.*;
import java.awt.*;

public class GuiKnoten extends JComponent {

    private static final ImageResource PATH_MARKER = ImageResource.PATH_MARKER;
    private static final ImageResource[][] UFO_IMAGES = initializeUfoImages();

    private static ImageResource[][] initializeUfoImages() {
        return new ImageResource[][]{ { ImageResource.UFO_BLUE,
                                        ImageResource.UFO_BLUE_MARKED },
                                      { ImageResource.UFO_RED,
                                        ImageResource.UFO_RED_MARKED },
                                      { ImageResource.UFO_GREEN,
                                        ImageResource.UFO_GREEN_MARKED },
                                      { ImageResource.UFO_YELLOW,
                                        ImageResource.UFO_YELLOW_MARKED } };
    }

    private Image image;

    public GuiKnoten() {
        setSize(32, 32);
    }

    public void update(int spieler, int status) {
        this.image = determineImage(spieler, status);
        repaint();
    }

    private Image determineImage(int spieler, int status) {
        Image image = null;
        if (spieler == -1) {
            image = status == 1 ? PATH_MARKER.load() : null;
        } else if (spieler < UFO_IMAGES.length && status < UFO_IMAGES[0].length) {
            image = UFO_IMAGES[spieler][status].load();
        }
        return image;
    }

    public void paintComponent(Graphics g) {
        if (image != null) {
            g.drawImage(image, 0, 0, 32, 32, this);
        }
    }

    public void locateAt(double x, double y) {
        setLocation(new Point(new Double(x).intValue(), new Double(y).intValue()));
    }

}
