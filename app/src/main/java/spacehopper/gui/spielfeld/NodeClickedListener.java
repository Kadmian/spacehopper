package spacehopper.gui.spielfeld;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spacehopper.core.Position;
import spacehopper.core.SpielerTyp;
import spacehopper.core.Spielverwaltung;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Volker
 */
public class NodeClickedListener extends MouseAdapter {

    private static final Logger LOG = LoggerFactory.getLogger("Spacehopper");

    private final Spielverwaltung spielverwaltung;
    private final Position position;

    public NodeClickedListener(Spielverwaltung spielverwaltung,
                               Position position) {
        this.spielverwaltung = spielverwaltung;
        this.position = position;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        LOG.trace("Node clicked: {}", position);
        spielverwaltung.klickeFeld(SpielerTyp.HUMAN, position);
    }
}
