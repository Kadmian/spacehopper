/*
 * SpielfeldSternPanel.java
 *
 * Created on March 12, 2004, 2:26 PM
 */

package spacehopper.gui.spielfeld;

import spacehopper.core.Position;
import spacehopper.core.Spielverwaltung;
import spacehopper.image.ImageResource;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sopr021
 */
public class SpielfeldQuadPanel extends SpielfeldPanel {

    public SpielfeldQuadPanel(Spielverwaltung spielverwaltung) {
        super(spielverwaltung, ImageResource.BOARD_SQUARE);
    }

    protected Map<Position, GuiKnoten> createSpielfeld(Spielverwaltung spielverwaltung) {
        Map<Position, GuiKnoten> feld = new HashMap<>();
        for (int rowIndex = 0; rowIndex < 16; rowIndex++) {
            for (int columnIndex = 0; columnIndex < 16; columnIndex++) {
                GuiKnoten knoten = new GuiKnoten();
                Position position = new Position(rowIndex, columnIndex);
                knoten.addMouseListener(new NodeClickedListener(spielverwaltung, position));
                feld.put(position, knoten);
                knoten.locateAt(9 + (columnIndex * 40), 10 + (rowIndex * 40));
            }
        }
        return feld;
    }
}
