/*
 * SpielfeldSternPanel.java
 *
 * Created on March 12, 2004, 2:26 PM
 */

package spacehopper.gui.spielfeld;

import spacehopper.core.Position;
import spacehopper.core.Spielverwaltung;
import spacehopper.image.ImageResource;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 * @author sopr021
 */
public abstract class SpielfeldPanel extends JPanel {

    private final Map<Position, GuiKnoten> feld;
    private final Image backgroundImage;

    protected SpielfeldPanel(Spielverwaltung spielverwaltung,
                             ImageResource backgroundImageResource) {
        this.backgroundImage = backgroundImageResource.load();

        this.setLayout(null);

        feld = createSpielfeld(spielverwaltung);
        feld.values().forEach(this::add);
    }

    protected abstract Map<Position, GuiKnoten> createSpielfeld(Spielverwaltung spielverwaltung);

    public void updateNode(Position position, int spieler, int status) {
        GuiKnoten guiKnoten = feld.get(position);
        guiKnoten.update(spieler, status);
    }

    public void paint(Graphics g) {
        paintComponent(g);
    }

    public void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 650, 650);
        if (backgroundImage != null) {
            g.drawImage(backgroundImage, 0, 0, this);
        }
        paintChildren(g);
    }

}
