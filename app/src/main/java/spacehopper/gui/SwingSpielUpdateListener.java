package spacehopper.gui;

import spacehopper.core.SpielStatus;
import spacehopper.core.SpielUpdateListener;

import java.awt.*;

public class SwingSpielUpdateListener implements SpielUpdateListener {

    private final MainPanel mainPanel;

    public SwingSpielUpdateListener(MainPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    @Override
    public void werteUpdateAus(SpielStatus spielStatus) {
        EventQueue.invokeLater(() -> mainPanel.werteUpdateAus(spielStatus));
    }
}
