package spacehopper.gui.spielerinfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spacehopper.core.SpielStatus;
import spacehopper.core.Spieler;
import spacehopper.core.SpielfeldTyp;
import spacehopper.image.ImageResource;
import spacehopper.infrastructure.ui.StyleGuide;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class SpielerInfoPanel extends JPanel {

    private static final Logger LOG = LoggerFactory.getLogger(SpielerInfoPanel.class);

    private static final ImageResource[] IMAGE_RESOURCES =
            {ImageResource.UFO_BLUE_ICON, ImageResource.UFO_RED_ICON,
             ImageResource.UFO_GREEN_ICON, ImageResource.UFO_YELLOW_ICON};

    private final List<SpielerInfo> spielerInfos;
    InfoIcon[] ufos;
    InfoIcon[] player;
    int activePlayer;
    int anzahl;
    private UUID currentGameId;

    public SpielerInfoPanel() {
        initComponents();
        ufos = new InfoIcon[4];
        player = new InfoIcon[4];

        spielerInfos = IntStream.range(0, 4)
                                .mapToObj(i -> new SpielerInfo(new Point(40,
                                                                         30 + i * 20)))
                                .peek(spielerInfo -> spielerInfo.addTo(this))
                                .collect(toList());
        for (int i = 0; i < 4; i++) {
            ufos[i] = new InfoIcon();
            ufos[i].setImage(IMAGE_RESOURCES[i]);
            ufos[i].setLocation(new Point(20, 30 + i * 20));
            ufos[i].setVisible(false);
            add(ufos[i]);

            player[i] = new InfoIcon();
            player[i].setLocation(new Point(150, 30 + i * 20));
            player[i].setVisible(false);
            add(player[i]);
        }
    }

    private void initComponents() {
        setLayout(null);
        StyleGuide.applyDefaultColors(this);
        setOpaque(false);
    }

    public void update(SpielStatus spielStatus) {
        if (!spielStatus.getSpielId().equals(currentGameId())) {
            initialize(spielStatus);
        } else {
            setActivePlayer(spielStatus.getAktiverSpielerIndex());
        }
    }

    public void initialize(SpielStatus spielStatus) {
        currentGameId = spielStatus.getSpielId();
        List<Spieler> teilnehmer = spielStatus.teilnehmer();
        SpielfeldTyp spielfeldTyp = spielStatus.spielfeldTyp();
        LOG.info("Initialisiere Spieler-Panel");
        if (teilnehmer.size() == 2 && spielfeldTyp == SpielfeldTyp.QUADRAT) {
            ufos[1].setImage(IMAGE_RESOURCES[2]);
        } else {
            ufos[1].setImage(IMAGE_RESOURCES[1]);
        }

        this.anzahl = teilnehmer.size();
        for (int spielerIndex = 0; spielerIndex < anzahl; spielerIndex++) {
            spielerInfos.get(spielerIndex).enable(teilnehmer.get(spielerIndex));
            ufos[spielerIndex].setVisible(true);
            ImageResource imageResource = determineImageResource(teilnehmer.get(
                    spielerIndex));
            player[spielerIndex].setImage(imageResource);

            player[spielerIndex].setVisible(true);
        }
        for (int i = anzahl; i < 4; i++) {
            spielerInfos.get(i).disable();
            ufos[i].setVisible(false);
            player[i].setVisible(false);
        }
        repaint();
    }

    private ImageResource determineImageResource(Spieler spieler) {
        ImageResource imageResource;
        switch (spieler.typ()) {
            case COMPUTER_EASY:
                imageResource = ImageResource.AVATAR_KI_EASY_ICON;
                break;
            case COMPUTER_MEDIUM:
                imageResource = ImageResource.AVATAR_KI_MEDIUM_ICON;
                break;
            case COMPUTER_HARD:
                imageResource = ImageResource.AVATAR_KI_HARD_ICON;
                break;
            case NETWORK_PLAYER:
                imageResource = ImageResource.AVATAR_NETWORK_ICON;
                break;
            case HUMAN:
            default:
                imageResource = ImageResource.AVATAR_HUMAN_ICON;
                break;
        }
        return imageResource;
    }

    public void setActivePlayer(int aktiverSpielerIndex) {
        if (!spielerInfos.isEmpty()
            && aktiverSpielerIndex > -1
            && aktiverSpielerIndex < spielerInfos.size()) {
            spielerInfos.get(activePlayer).deselect();
            spielerInfos.get(aktiverSpielerIndex).select();
            activePlayer = aktiverSpielerIndex;
        }
    }

    private UUID currentGameId() {
        return currentGameId;
    }

    public static class InfoIcon extends JComponent {
        ImageResource image;

        public InfoIcon() {
            setSize(16, 16);
        }

        public void paintComponent(Graphics g) {
            if (image != null) {
                g.drawImage(image.load(), 0, 0, 16, 16, this);
            }
        }

        public void setImage(ImageResource imageResource) {
            this.image = imageResource;
        }
    }
}
