package spacehopper.gui.spielerinfo;

import spacehopper.core.Spieler;
import spacehopper.infrastructure.ui.ComponentFactory;

import javax.swing.*;
import java.awt.*;

/**
 * @author Volker
 */
public class SpielerInfo {

    private final JLabel name;

    public SpielerInfo(Point location) {
        name = ComponentFactory.label();
        name.setLocation(location);
        name.setSize(100, 16);
        name.setVisible(false);
        name.setOpaque(true);
    }

    public void enable(Spieler spieler) {
        name.setText("   " + spieler.getName());
        name.setVisible(true);
    }

    public void disable() {
        name.setVisible(false);
    }

    public void addTo(JComponent parentComponent) {
        parentComponent.add(name);
    }

    public void deselect() {
        name.setBackground(Color.BLACK);
    }

    public void select() {
        name.setBackground(Color.BLUE);
    }
}
