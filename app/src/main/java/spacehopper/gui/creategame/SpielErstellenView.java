package spacehopper.gui.creategame;

import spacehopper.core.Spieler;
import spacehopper.core.SpielerTyp;
import spacehopper.core.SpielfeldTyp;
import spacehopper.gui.MainPanel;
import spacehopper.infrastructure.ui.StyleGuide;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static spacehopper.infrastructure.ui.StyleGuide.titledBorder;

public class SpielErstellenView extends JFrame {

    private final MainPanel mainPanel;

    private PlayerComponent player1;
    private PlayerComponent player2;
    private PlayerComponent player3;
    private PlayerComponent player4;
    private SpielfeldComponent spielfeldComponent;

    public SpielErstellenView(MainPanel mainPanel) {
        super("Neues Spiel erstellen");
        this.mainPanel = mainPanel;
        initComponents();
    }

    private void initComponents() {
        player1 = new PlayerComponent("Name1", SpielerTyp.HUMAN);
        player2 = new PlayerComponent();
        player3 = new PlayerComponent();
        player4 = new PlayerComponent();
        player4.onTypChanged(e -> onSpieler4TypChanged());
        JPanel spielerPanel = createSpielerPanel(player1, player2, player3, player4);

        spielfeldComponent = new SpielfeldComponent();
        spielfeldComponent.register(this::onSpielfeldTypChanged);

        JPanel contentPanel = StyleGuide.defaultPanel(new EtchedBorder());
        addInBounds(contentPanel, spielerPanel, 10, 10, 350, 420);
        addInBounds(contentPanel, spielfeldComponent.panel(), 10, 440, 350, 90);
        addInBounds(contentPanel, createButtonPanel(), 380, 20, 120, 510);
        getContentPane().add(contentPanel, BorderLayout.CENTER);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width - 537) / 2, (screenSize.height - 586) / 2, 537, 586);
    }

    private JPanel createSpielerPanel(PlayerComponent... players) {
        JPanel spielerPanel = StyleGuide.defaultPanel(titledBorder("Spieler"));
        for (int i = 0; i < players.length; i++) {
            addPlayerToPanel(spielerPanel, i, "Spieler " + (i + 1) + ":", players[i]);
        }
        return spielerPanel;
    }

    private void addPlayerToPanel(JPanel spielerPanel,
                                  int spielerIndex,
                                  String labelText,
                                  PlayerComponent psc) {
        JLabel label = new JLabel(labelText);
        addInBounds(spielerPanel, label, 10, 20 + spielerIndex * 100, 60, 30);
        addInBounds(spielerPanel,
                    psc.panel(),
                    80,
                    20 + spielerIndex * 100,
                    psc.width(),
                    psc.height());
    }

    private void addInBounds(Container container,
                             Component component,
                             int x,
                             int y,
                             int width,
                             int height) {
        container.add(component);
        component.setBounds(x, y, width, height);
    }

    private JPanel createButtonPanel() {
        JButton okButton = createButton("OK", this::okActionPerformed);
        JButton abbrechenButton =
                createButton("Abbrechen", this::abbrechenActionPerformed);

        JPanel buttonPanel = StyleGuide.defaultPanel(new EtchedBorder());
        addInBounds(buttonPanel, okButton, 10, 10, 100, 25);
        addInBounds(buttonPanel, abbrechenButton, 10, 460, 100, 25);
        return buttonPanel;
    }

    private JButton createButton(String buttonText, ActionListener actionListener) {
        JButton ok = new JButton();
        ok.setBackground(Color.BLACK);
        ok.setForeground(Color.WHITE);
        ok.setText(buttonText);
        ok.addActionListener(actionListener);
        return ok;
    }

    private void okActionPerformed(ActionEvent evt) {
        if (spielerReihenfolgeIstUngueltig()) {
            showMessage("Ungueltige Spielerreihenfolge!");
            return;
        }

        long spieleranzahl = bestimmeSpielerzahl();

        if (spieleranzahl == 0) {
            showMessage("Keine Spieler!");
            return;
        }

        SpielfeldTyp spielfeldTyp = spielfeldComponent.getSelectedTyp();

        if (combinationIsValid(spielfeldTyp, spieleranzahl)) {
            startNewGame(spielfeldTyp);
            dispose();
        } else {
            showMessage("Ungueltige Kombination von Spieleranzahl und Bretttyp.");
        }
    }

    private boolean spielerReihenfolgeIstUngueltig() {
        return (player4.isConfigured()) && (!player3.isConfigured()
                                            || !player2.isConfigured()
                                            || !player1.isConfigured())
               || (player3.isConfigured()) && (!player2.isConfigured()
                                               || !player1.isConfigured())
               || ((player2.isConfigured()) && !player1.isConfigured());
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    private long bestimmeSpielerzahl() {
        return Stream.of(player1, player2, player3, player4)
                     .filter(PlayerComponent::isConfigured)
                     .count();
    }

    private boolean combinationIsValid(SpielfeldTyp spielfeldTyp, long spieleranzahl) {
        return !(SpielfeldTyp.QUADRAT.equals(spielfeldTyp) && spieleranzahl == 3);
    }

    private void startNewGame(SpielfeldTyp spielfeldTyp) {
        List<Spieler> teilnehmer = Stream.of(player1, player2, player3, player4)
                                         .filter(PlayerComponent::isConfigured)
                                         .map(PlayerComponent::getSpieler)
                                         .filter(Optional::isPresent)
                                         .map(Optional::get)
                                         .collect(toList());

        mainPanel.newGame(teilnehmer, spielfeldTyp);
    }

    private void abbrechenActionPerformed(ActionEvent evt) {
        dispose();
    }

    private void onSpieler4TypChanged() {
        if (player4.isConfigured()) {
            spielfeldComponent.select(SpielfeldTyp.QUADRAT);
        }
    }

    private void onSpielfeldTypChanged(SpielfeldTyp neuerSpielfeldTyp) {
        if (SpielfeldTyp.STERN.equals(neuerSpielfeldTyp)) {
            player4.clear();
        } else if (SpielfeldTyp.QUADRAT.equals(neuerSpielfeldTyp)) {
            player4.enable();
        }
    }

}
