package spacehopper.gui.creategame;

import spacehopper.core.Spieler;
import spacehopper.core.SpielerProfil;
import spacehopper.core.SpielerTyp;
import spacehopper.image.ImageResource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Optional;

/**
 * @author Volker
 */
public class PlayerComponent {

    private static final int TYPE_WIDTH = 140;
    private static final int TYPE_HEIGHT = 30;
    private static final int NAME_WIDTH = 140;
    private static final int NAME_HEIGHT = 30;
    private static final int AVATAR_WIDTH = 64;
    private static final int AVATAR_HEIGHT = 64;

    private final JComboBox<SpielerTyp> spielerTyp;
    private final JTextField spielerName;
    private final JLabel avatar;

    public PlayerComponent(String spielerName, SpielerTyp spielerTyp) {
        this();
        setName(spielerName);
        setTyp(spielerTyp);
    }

    public PlayerComponent() {
        this.spielerTyp = spielerTypComboBox();
        this.spielerName = createSpielerName();
        this.avatar = new JLabel();
        this.spielerTyp.addActionListener(this::onSpielerTypChanged);
        updateAvatar();
    }

    private void onSpielerTypChanged(ActionEvent e) {
        updateAvatar();
        setDefaultSpielername();
    }

    private JTextField createSpielerName() {
        JTextField spielerName = new JTextField();
        spielerName.setFont(namensFeldFont());
        spielerName.setText("");
        return spielerName;
    }

    private void updateAvatar() {
        if (!hasSelection()) {
            avatar.setIcon(null);
        } else if (spielerTyp.getSelectedIndex() == 1) {
            avatar.setIcon(ImageResource.AVATAR_HUMAN.loadImageIcon());
        } else if (spielerTyp.getSelectedIndex() == 2) {
            avatar.setIcon(ImageResource.AVATAR_KI_EASY.loadImageIcon());
        } else if (spielerTyp.getSelectedIndex() == 3) {
            avatar.setIcon(ImageResource.AVATAR_KI_MEDIUM.loadImageIcon());
        } else if (spielerTyp.getSelectedIndex() == 4) {
            avatar.setIcon(ImageResource.AVATAR_KI_HARD.loadImageIcon());
        } else if (spielerTyp.getSelectedIndex() == 5) {
            avatar.setIcon(ImageResource.AVATAR_NETWORK.loadImageIcon());
        }
    }

    private boolean hasSelection() {
        return spielerTyp.getSelectedItem() != null;
    }

    private Font namensFeldFont() {
        return new Font("Dialog", Font.BOLD, 12);
    }

    private void setDefaultSpielername() {
        String spielerName = bestimmeDefaultSpielernamen();
        setName(spielerName);
    }

    private String bestimmeDefaultSpielernamen() {
        String spielerName = "";
        if (SpielerTyp.COMPUTER_EASY.equals(spielerTyp.getSelectedItem())) {
            spielerName = "Bender";
        } else if (SpielerTyp.COMPUTER_MEDIUM.equals(spielerTyp.getSelectedItem())) {
            spielerName = "DevilBot";
        } else if (SpielerTyp.COMPUTER_HARD.equals(spielerTyp.getSelectedItem())) {
            spielerName = "Santa";
        }
        return spielerName;
    }

    public JPanel panel() {
        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);

        panel.add(spielerTyp);
        spielerTyp.setBounds(0, 0, TYPE_WIDTH, TYPE_HEIGHT);

        panel.add(spielerName);
        spielerName.setBounds(0, 40, NAME_WIDTH, NAME_HEIGHT);

        panel.add(avatar);
        avatar.setBounds(170, 0, AVATAR_WIDTH, AVATAR_HEIGHT);

        return panel;
    }

    public int width() {
        return 170 + AVATAR_WIDTH;
    }

    public int height() {
        return 40 + NAME_HEIGHT;
    }

    public void setName(String spielerName) {
        this.spielerName.setText(spielerName);
    }

    public void enable() {
        spielerName.setEnabled(true);
    }

    private JComboBox<SpielerTyp> spielerTypComboBox() {
        JComboBox<SpielerTyp> spielerTypComboBox = new JComboBox<>();
        spielerTypComboBox.setBackground(Color.BLACK);
        spielerTypComboBox.setForeground(Color.WHITE);
        spielerTypComboBox.setFont(new Font("Dialog", Font.BOLD, 11));
        spielerTypComboBox.setModel(new DefaultComboBoxModel<>(new SpielerTyp[]{ null,
                                                                                 SpielerTyp.HUMAN,
                                                                                 SpielerTyp.COMPUTER_EASY,
                                                                                 SpielerTyp.COMPUTER_MEDIUM,
                                                                                 SpielerTyp.COMPUTER_HARD,
                                                                                 SpielerTyp.NETWORK_PLAYER }));
        spielerTypComboBox.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list,
                                                          Object value,
                                                          int index,
                                                          boolean isSelected,
                                                          boolean cellHasFocus) {
                String convertedValue = convert(value);
                return super.getListCellRendererComponent(list,
                                                          convertedValue,
                                                          index,
                                                          isSelected,
                                                          cellHasFocus);
            }

            private String convert(Object value) {
                if (SpielerTyp.HUMAN.equals(value)) {
                    return "Humanoid";
                } else if (SpielerTyp.NETWORK_PLAYER.equals(value)) {
                    return "Netzwerkspieler";
                } else if (SpielerTyp.COMPUTER_EASY.equals(value)) {
                    return "Android v1";
                } else if (SpielerTyp.COMPUTER_MEDIUM.equals(value)) {
                    return "Android v2";
                } else if (SpielerTyp.COMPUTER_HARD.equals(value)) {
                    return "Android v3";
                } else if (value == null) {
                    return "- leer -";
                } else {
                    return "!!!";
                }
            }
        });
        return spielerTypComboBox;
    }

    public void setTyp(SpielerTyp typ) {
        spielerTyp.setSelectedItem(typ);
    }

    public void onTypChanged(ActionListener actionListener) {
        spielerTyp.addActionListener(actionListener);
    }

    public Optional<Spieler> getSpieler() {
        if (isConfigured()) {
            SpielerTyp typ = spielerTyp.getItemAt(spielerTyp.getSelectedIndex());
            SpielerProfil profil = new SpielerProfil(spielerName.getText(), typ);
            return Optional.of(new Spieler(profil));
        } else {
            return Optional.empty();
        }
    }

    public void clear() {
        setTyp(null);
    }

    public boolean isConfigured() {
        return spielerTyp.getSelectedItem() != null;
    }
}
