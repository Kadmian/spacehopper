package spacehopper.gui.creategame;

import spacehopper.core.SpielfeldTyp;
import spacehopper.image.ImageResource;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Volker
 */
public class SpielfeldComponent {

    private final JRadioButton sternRadio;
    private final JRadioButton quadRadio;
    private final JLabel vorschau;

    private final List<SelectedTypeChangedListener> listeners = new LinkedList<>();
    private SpielfeldTyp selectedTyp;

    public SpielfeldComponent() {
        this.sternRadio = createRadioButton("Sternfoermiges Spielfeld",
                                            e -> select(SpielfeldTyp.STERN));
        this.quadRadio = createRadioButton("Quadratisches Spielfeld",
                                           e -> select(SpielfeldTyp.QUADRAT));
        group(this.sternRadio, this.quadRadio);
        this.vorschau = vorschauLabel();
        select(SpielfeldTyp.STERN);
    }

    private JRadioButton createRadioButton(String buttonText,
                                           ActionListener buttonAction) {
        JRadioButton radioButton = new JRadioButton();
        radioButton.setBackground(Color.BLACK);
        radioButton.setForeground(Color.WHITE);
        radioButton.setText(buttonText);
        radioButton.addActionListener(buttonAction);
        radioButton.setSelected(false);
        return radioButton;
    }

    private void group(JRadioButton sternRadio, JRadioButton quadRadio) {
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(sternRadio);
        buttonGroup.add(quadRadio);
    }

    private JLabel vorschauLabel() {
        JLabel vorschau = new JLabel();
        vorschau.setBackground(Color.BLACK);
        vorschau.setIcon(ImageResource.BOARD_STAR_ICON.loadImageIcon());
        vorschau.setText("Spielfeld-Vorschau");
        vorschau.setOpaque(true);
        return vorschau;
    }

    public void select(SpielfeldTyp selectedTyp) {
        if (selectedTyp != null && !selectedTyp.equals(this.selectedTyp)) {
            this.selectedTyp = selectedTyp;
            ensureCorrectRadioButtonIsSelected();
            updateVorschau();
            notifyListenersSelectedTypeChanged(selectedTyp);
        }
    }

    private void ensureCorrectRadioButtonIsSelected() {
        if (SpielfeldTyp.STERN.equals(selectedTyp)) {
            if (!sternRadio.isSelected()) {
                sternRadio.setSelected(true);
            }
        } else if (SpielfeldTyp.QUADRAT.equals(selectedTyp)) {
            if (!quadRadio.isSelected()) {
                quadRadio.setSelected(true);
            }
        }
    }

    private void updateVorschau() {
        if (SpielfeldTyp.STERN.equals(selectedTyp)) {
            vorschau.setIcon(ImageResource.BOARD_STAR_ICON.loadImageIcon());
        } else {
            vorschau.setIcon(ImageResource.BOARD_SQUARE_ICON.loadImageIcon());
        }
    }

    private void notifyListenersSelectedTypeChanged(SpielfeldTyp selectedTyp) {
        listeners.forEach(listener -> listener.onSelectedTypeChanged(selectedTyp));
    }

    public Component panel() {
        JPanel spielfeldAuswahl = createDefaultPanel(titledBorder("Spielfeld"));
        spielfeldAuswahl.add(sternRadio);
        sternRadio.setBounds(10, 20, 170, 23);
        spielfeldAuswahl.add(quadRadio);
        quadRadio.setBounds(10, 50, 170, 23);
        spielfeldAuswahl.add(vorschau);
        vorschau.setBounds(250, 20, 64, 64);

        return spielfeldAuswahl;
    }

    private JPanel createDefaultPanel(Border border) {
        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        panel.setBorder(border);
        return panel;
    }

    private TitledBorder titledBorder(String title) {
        return new TitledBorder(null,
                                title,
                                TitledBorder.DEFAULT_JUSTIFICATION,
                                TitledBorder.DEFAULT_POSITION,
                                borderTitleFont(),
                                Color.WHITE);
    }

    private Font borderTitleFont() {
        return new Font("Dialog", Font.PLAIN, 11);
    }

    public SpielfeldTyp getSelectedTyp() {
        return selectedTyp;
    }

    public void register(SelectedTypeChangedListener listener) {
        listeners.add(listener);
    }

    public interface SelectedTypeChangedListener {
        void onSelectedTypeChanged(SpielfeldTyp newSelectedType);
    }
}
