package spacehopper.gui.spielstatus;

import spacehopper.core.SpielStatus;

import javax.swing.*;
import java.awt.*;

/**
 * @author Volker
 */
public class SpielStatusPanel extends JPanel {

    private static final String[] COMMENT_MESSAGES = { "Figur ausgewaehlt",
                                                       "Ungueltige Figur!",
                                                       "Ungueltiges Feld!",
                                                       "Andere Figur waehlen!",
                                                       "Zug beendet",
                                                       "Zug zurueckgenommen",
                                                       "Zug kann nicht zurueckgenommen "
                                                       + "werden!",
                                                       "Teilzug zurueckgenommen",
                                                       "Zug erweitert",
                                                       "Spiel bereit",
                                                       "Spiel beendet",
                                                       "Zug vorgegangen" };

    private JLabel rundenAnzeige;
    private JLabel comment;

    public SpielStatusPanel() {
        super();
        setLayout(null);
        setBackground(Color.BLACK);
        setForeground(Color.WHITE);
        setOpaque(false);

        initialisiereRundenAnzeige();
        initialisiereKommentar();
    }

    private void initialisiereRundenAnzeige() {
        JLabel rundenLabel = new JLabel();
        rundenLabel.setForeground(Color.WHITE);
        rundenLabel.setText("Runde:");
        add(rundenLabel);
        rundenLabel.setBounds(10, 20, 45, 15);
        rundenAnzeige = new JLabel();
        rundenAnzeige.setForeground(Color.WHITE);
        add(rundenAnzeige);
        rundenAnzeige.setBounds(65, 17, 40, 20);
    }

    private void initialisiereKommentar() {
        JLabel commentLabel = new JLabel();
        commentLabel.setForeground(Color.WHITE);
        commentLabel.setText("Kommentar:");
        add(commentLabel);
        commentLabel.setBounds(10, 40, 80, 15);

        comment = new JLabel();
        this.comment.setFont(new Font("Dialog", Font.BOLD, 10));
        this.comment.setForeground(Color.WHITE);
        add(this.comment);
        this.comment.setBounds(20, 60, 150, 20);
    }

    public void update(SpielStatus status) {
        aktualisiereRundenAnzeige(status.getRunde());
        aktualisiereKommentar(status);
    }

    private void aktualisiereRundenAnzeige(int runde) {
        this.rundenAnzeige.setText(Integer.toString(runde));
    }

    private void aktualisiereKommentar(SpielStatus status) {
        String kommentar = bestimmeKommentar(status);
        aktualisiereKommentar(kommentar);
    }

    private String bestimmeKommentar(SpielStatus update) {
        String message = "";
        if (update.getGewonnen()) {
            message = update.getSpielerNamen()[update.getAktiverSpielerIndex()]
                      + " hat gewonnen!";
        } else if (update.getStatusMessage() != null) {
            message = COMMENT_MESSAGES[update.getStatusMessage().legacyMessageId() - 1];
        }
        return message;
    }

    private void aktualisiereKommentar(String message) {
        comment.setText(message);
    }

}
