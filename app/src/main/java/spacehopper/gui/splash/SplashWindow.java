package spacehopper.gui.splash;

import java.awt.*;

public class SplashWindow extends Window implements Runnable {

    private final StringBuffer message;
    private Thread runner;
    private Image splashIm;
    private Image Buffer;
    private Graphics gBuffer;
    private int animationStep;

    public SplashWindow(Frame parent, Image splashIm, String message) {
        super(parent);
        this.splashIm = splashIm;
        this.message = new StringBuffer(message);
        setSize(400, 300);
        animationStep = 0;
        Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle winDim = getBounds();
        setLocation((screenDim.width - winDim.width) / 2,
                    (screenDim.height - winDim.height) / 2);
        setVisible(true);
        toFront();
        start();
    }

    public void start() {
        runner = new Thread(this);
        runner.start();
    }

    public void addNotify() {
        super.addNotify();
        Buffer = createImage(400, 300);
        gBuffer = Buffer.getGraphics();
    }

    public void run() {
        try {
            for (; !Thread.interrupted(); Thread.sleep(15L)) {
                buildBuffer();
                paint(getGraphics());
                animationStep -= 8;
            }

        } catch (InterruptedException e) {
        }
    }

    public void buildBuffer() {
        if (splashIm != null) {
            gBuffer.setColor(Color.black);
            gBuffer.fillRect(0, 0, 400, 300);
            gBuffer.drawImage(splashIm, 0, 0, this);
            drawAnimation();
            gBuffer.setColor(Color.lightGray);
            gBuffer.drawString(message.toString(), 20, 290);
        }
    }

    public void drawAnimation() {
        if (gBuffer == null) {
            return;
        }
        if (animationStep < 0) {
            animationStep = 399;
        }
        for (int i = 0; i < 400; i++) {
            int thisColor;
            for (thisColor = (int) (((float) (animationStep + i) / 400F) * 256F);
                 thisColor > 255;
                 thisColor -= 256) {
            }
            for (; thisColor < 0; thisColor += 256) {
            }
            gBuffer.setColor(new Color(0, 0, (2 * thisColor) / 3));
            gBuffer.drawLine(i, 278, i, 293);
        }

    }

    public void paint(Graphics g) {
        if (Buffer != null && g != null) {
            g.drawImage(Buffer, 0, 0, this);
        }
    }

    protected void finalize() {
        stop();
        splashIm.flush();
        splashIm = null;
        Buffer.flush();
        Buffer = null;
    }

    public void stop() {
        runner.interrupt();
        runner = null;
    }
}
