/*
 * splash.java
 *
 * Created on March 10, 2004, 4:00 PM
 */

/**
 * @author pat
 */
package spacehopper.gui.splash;

import spacehopper.image.ImageResource;

import javax.swing.*;
import java.awt.*;

public class Splash {

    private final JFrame frame;
    private SplashWindow sw;

    /**
     * Creates a new instance of splash
     */
    public Splash() {
        frame = new JFrame("");
    }

    public void start() {
        frame.setSize(400, 300);
        Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle frameDim = frame.getBounds();
        frame.setLocation((screenDim.width - frameDim.width) / 2,
                          (screenDim.height - frameDim.height) / 2);
        MediaTracker mt = new MediaTracker(frame);
        Image splashIm = frame.getToolkit()
                              .getImage(ImageResource.class.getResource("splashlogo.jpg"));
        mt.addImage(splashIm, 0);
        try {
            mt.waitForID(0);
            sw = new SplashWindow(frame, splashIm, " Spacehopper im Orbit...");
            Thread.sleep(30);
        } catch (InterruptedException ie) {
        }
    }

    public void stop() {
        sw.dispose();
        frame.dispose();
    }
}
