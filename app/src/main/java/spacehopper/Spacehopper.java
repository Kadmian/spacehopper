package spacehopper;

/*
 * Spacehopper.java
 *
 * Created on March 15, 2004, 6:31 PM
 */

import spacehopper.app.SpielEndeListener;
import spacehopper.core.SpielStatus;
import spacehopper.core.Spielverwaltung;
import spacehopper.gui.MainPanel;
import spacehopper.gui.SwingSpielUpdateListener;
import spacehopper.gui.splash.Splash;
import spacehopper.help.HilfeMenuProvider;
import spacehopper.highscore.HighscoreMenuProvider;

public class Spacehopper {

    public static void main(String[] args) {
        Splash splash = new Splash();
        splash.start();
        Spielverwaltung spielverwaltung = new Spielverwaltung();
        spielverwaltung.register(new SpielEndeListener());
        MainPanel mainPanel = createMainView(spielverwaltung);

        spielverwaltung.register(new SwingSpielUpdateListener(mainPanel));

        SpielStatus autoSaveGame = spielverwaltung.loadAutosaveGame();
        if (!autoSaveGame.getGewonnen()) {
            mainPanel.initializeWith(autoSaveGame);
        }

        mainPanel.setVisible(true);
        splash.stop();
    }

    private static MainPanel createMainView(Spielverwaltung sv) {
        HighscoreMenuProvider highscoreMenuProvider = new HighscoreMenuProvider();
        HilfeMenuProvider hilfeMenuProvider = new HilfeMenuProvider();
        return new MainPanel(sv, highscoreMenuProvider, hilfeMenuProvider);
    }

}
