/*
 * Server.java
 *
 * Created on March 9, 2004, 8:21 AM
 */
package spacehopper.netzwerk;
/**
 * @author pat
 */

import spacehopper.core.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class NetzwerkServer extends Thread {
    private final static String PROTOKOLL_HEADER = "HAPRA - Halma Protocol A(0815)";
    private final static String VERBINDUNGS_OK = "OK";
    private final static String[] PROTOKOLL_BRETT = { "Stern", "Quadrat" };
    private final static String MSG_SIGNAL = "MSG ";
    private final static String START_SIGNAL = "START";
    private final static String RUNDEN_SIGNAL = "MARS ";
    private final static String DRAN_SIGNAL = "DRAN";
    private final static String ZUG_SIGNAL = "ZUG";

    private final boolean DEBUG = false;
    private final Spielverwaltung spielverwaltung;
    private final ComObject comObject;
    private final int brettTyp;
    private final String[] spielerNamen;
    private final int[] spielerTypen;
    private final int clickDelay;
    private final Transform transformer;
    private ServerSocket listenSocket;
    private Socket[] clientSocket;
    private BufferedReader[] clientBufferedReader;
    private BufferedWriter[] clientBufferedWriter;
    private int[] spielerNummer;
    private int[] socketNummer;
    private boolean laeuft;

    /**
     * Creates a new instance of Server
     */
    public NetzwerkServer(Spielverwaltung spielverwaltung,
                          ComObject comObject,
                          int brettTyp,
                          String[] spielerNamen,
                          int[] spielerTypen) {
        this.spielverwaltung = spielverwaltung;
        this.comObject = comObject;
        this.brettTyp = brettTyp;
        this.spielerNamen = spielerNamen;
        this.spielerTypen = spielerTypen;
        clickDelay = 200;
        transformer = new Transform();
    }

    /**
     * Creates a new instance of WebServer
     */
    public void run() {
        laeuft = true;

        try {
            listenSocket = new ServerSocket(1821);
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }

        int netzwerkSpielerAnzahl = 0;
        socketNummer = new int[spielerTypen.length];
        int j = 0;
        for (int i = 0; i < spielerTypen.length; i++) {
            if (spielerTypen[i] == 4) {
                netzwerkSpielerAnzahl++;
                socketNummer[i] = j++;
            }
        }

        spielerNummer = new int[netzwerkSpielerAnzahl];
        j = 0;
        for (int i = 0; i < spielerTypen.length; i++) {
            if (spielerTypen[i] == 4) {
                spielerNummer[j++] = i;
            }
        }

        clientSocket = new Socket[netzwerkSpielerAnzahl];
        clientBufferedReader = new BufferedReader[netzwerkSpielerAnzahl];
        clientBufferedWriter = new BufferedWriter[netzwerkSpielerAnzahl];

        int socketZahl = 0;
        while (socketZahl < netzwerkSpielerAnzahl && laeuft) {
            try {
                clientSocket[socketZahl] = listenSocket.accept();
                clientBufferedReader[socketZahl] = new BufferedReader(new InputStreamReader(
                        clientSocket[socketZahl].getInputStream()));
                clientBufferedWriter[socketZahl] = new BufferedWriter(new OutputStreamWriter(
                        clientSocket[socketZahl].getOutputStream()));
            } catch (Exception e) {
                if (DEBUG) {
                    e.printStackTrace();
                }
            }

            sende(PROTOKOLL_HEADER, socketZahl);
            spielerNamen[spielerNummer[socketZahl]] = empfange(socketZahl);
            sende(Integer.toString(spielerNummer[socketZahl] + 1), socketZahl);
            sende(VERBINDUNGS_OK, socketZahl);
            sende(PROTOKOLL_BRETT[brettTyp], socketZahl);
            sende(MSG_SIGNAL + "Es befinden sich " + (spielerTypen.length - netzwerkSpielerAnzahl
                                                      + socketZahl
                                                      + 1) + " Spieler im Spiel");

            socketZahl++;
        }
        try {
            listenSocket.close();
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }

        spielverwaltung.newGameStart(spielerTypen,
                                     spielerNamen,
                                     SpielfeldTyp.byLegacyType(brettTyp)
                                                 .orElse(SpielfeldTyp.QUADRAT));

        sende(START_SIGNAL);
        sende(Integer.toString(spielerTypen.length));
        for (int i = 0; i < spielerTypen.length; i++) {
            sende(spielerNamen[i]);
            switch (i) {
                case 0:
                    sende("#0000FF");
                    break;
                case 1:
                    sende("#FF0000");
                    break;
                case 2:
                    sende("#00FF00");
                    break;
                case 3:
                    sende("#FFFF00");
                    break;
            }
        }

        int dran = 0;
        Zug zug = null;
        while (laeuft && alleVerbunden()) {
            sende(RUNDEN_SIGNAL + (dran + 1));
            if (spielerTypen[dran] == 4) {

                List<Position> clicks = null;
                while (clicks == null) {
                    resetBuffer(socketNummer[dran]);
                    sende(DRAN_SIGNAL, socketNummer[dran]);

                    zug = empfangeZug(socketNummer[dran]);

                    clicks = spielverwaltung.erreichbar(zug);
                }
                sendeZug(zug, dran);

                for (Position click : clicks) {
                    spielverwaltung.klickeFeld(SpielerTyp.NETWORK_PLAYER, click);
                    try {
                        sleep(clickDelay);
                    } catch (Exception e) {
                        if (DEBUG) {
                            e.printStackTrace();
                        }
                    }
                }
                Position position = clicks.get(clicks.size() - 1);
                spielverwaltung.klickeFeld(SpielerTyp.NETWORK_PLAYER, position);
            } else {
                while (true) {
                    if ((zug = comObject.getZug()) != null) {
                        sendeZug(zug, dran);
                        break;
                    }
                    try {
                        sleep(50);
                    } catch (Exception e) {
                        if (DEBUG) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            dran = (dran + 1) % spielerTypen.length;
        }
    }

    private Zug empfangeZug(int i) {
        try {
            int x1 = Integer.parseInt(empfange(i));
            int y1 = Integer.parseInt(empfange(i));
            int x2 = Integer.parseInt(empfange(i));
            int y2 = Integer.parseInt(empfange(i));

            Position start = (brettTyp == 0)
                             ? transformer.alien2native(x1, y1)
                             : new Position(y1, x1);
            Position end = (brettTyp == 0)
                           ? transformer.alien2native(x2, y2)
                           : new Position(y2, x2);
            return new Zug().erweitertUm(start).erweitertUm(end);
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void sendeZug(Zug zug, int num) {
        try {
            for (int i = 0; i < clientBufferedWriter.length; i++) {
                if (clientBufferedWriter[i] != null) {
                    clientBufferedWriter[i].write(ZUG_SIGNAL + "\n");
                    clientBufferedWriter[i].write(((brettTyp == 0)
                                                   ? transformer.native2alien(zug.startpunkt()).x
                                                   : zug.startpunkt().y()) + "\n");
                    clientBufferedWriter[i].write(((brettTyp == 0)
                                                   ? transformer.native2alien(zug.startpunkt()).y
                                                   : zug.startpunkt().x()) + "\n");
                    clientBufferedWriter[i].write(((brettTyp == 0)
                                                   ? transformer.native2alien(zug.endpunkt()).x
                                                   : zug.endpunkt().y()) + "\n");
                    clientBufferedWriter[i].write(((brettTyp == 0)
                                                   ? transformer.native2alien(zug.endpunkt()).y
                                                   : zug.endpunkt().x()) + "\n");
                    clientBufferedWriter[i].write((num + 1) + "\n");
                    clientBufferedWriter[i].flush();
                }
            }
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private void sende(String command, int i) {
        try {
            clientBufferedWriter[i].write(command + "\n");
            clientBufferedWriter[i].flush();
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private void sende(String command) {
        try {
            for (int i = 0; i < clientBufferedWriter.length; i++) {
                if (clientBufferedWriter[i] != null) {
                    clientBufferedWriter[i].write(command + "\n");
                    clientBufferedWriter[i].flush();
                }
            }
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private void resetBuffer(int i) {
        try {
            clientBufferedReader[i].reset();
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private String empfange(int i) {
        try {
            String result = clientBufferedReader[i].readLine();
            //System.out.println(" -- Server sagt: "+ result);
            return result;
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private boolean alleVerbunden() {
        boolean verbunden = true;
        for (int i = 0; i < clientSocket.length; i++) {
            if (clientSocket[i].isClosed()) {
                verbunden = false;
            }
        }
        return verbunden;
    }

    public void beenden() {
        laeuft = false;
        try {
            for (int i = 0; i < clientBufferedReader.length; i++) {
                clientBufferedWriter[i].close();
                clientBufferedReader[i].close();
                clientSocket[i].close();
            }
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
    }
}
