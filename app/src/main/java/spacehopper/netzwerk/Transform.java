package spacehopper.netzwerk;

import spacehopper.core.Position;

import java.awt.*;


public class Transform {

    private final int[] aoffset;
    private final int[] boffset;

    public Transform() {
        aoffset = new int[17];

        aoffset[0] = 5;
        aoffset[1] = 5;
        aoffset[2] = 5;
        aoffset[3] = 5;
        aoffset[4] = 1;
        aoffset[5] = 2;
        aoffset[6] = 3;
        aoffset[7] = 4;
        aoffset[8] = 5;
        aoffset[9] = 5;
        aoffset[10] = 5;
        aoffset[11] = 5;
        aoffset[12] = 5;
        aoffset[13] = 10;
        aoffset[14] = 11;
        aoffset[15] = 12;
        aoffset[16] = 13;

        boffset = new int[17];
        boffset[0] = 5;
        boffset[1] = 6;
        boffset[2] = 7;
        boffset[3] = 8;
        boffset[4] = 13;
        boffset[5] = 13;
        boffset[6] = 13;
        boffset[7] = 13;
        boffset[8] = 13;
        boffset[9] = 14;
        boffset[10] = 15;
        boffset[11] = 16;
        boffset[12] = 17;
        boffset[13] = 13;
        boffset[14] = 13;
        boffset[15] = 13;
        boffset[16] = 13;

    }

    public Point native2alien(Position punkt) {
        return native2alien(punkt.x(), punkt.y());
    }

    public Point native2alien(int a, int b) {
        return new Point(aoffset[a] + b, boffset[a] - b);
    }

    public Position alien2native(Point punkt) {
        return alien2native(punkt.x, punkt.y);
    }

    public Position alien2native(int x, int y) {

        int zeile = -1;
        int spalte = -1;

        boolean gefunden = false;

        for (zeile = 0; zeile < 17 && !gefunden; zeile++) {
            for (spalte = 0; spalte < 13 && !gefunden; spalte++) {
                if (((aoffset[zeile] + spalte) == x) && ((boffset[zeile] - spalte) == y)) {
                    gefunden = true;
                }
            }
        }


        if (gefunden) {
            return new Position(zeile - 1, spalte - 1);
        } else {
            return null;
        }
    }
}
