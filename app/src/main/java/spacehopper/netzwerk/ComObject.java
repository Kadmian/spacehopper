package spacehopper.netzwerk;

import spacehopper.core.Zug;

/*
 * ComObject.java
 *
 * Created on March 24, 2004, 2:06 PM
 */

/**
 * @author sopr023
 */
public class ComObject {

    public static final int NETZTWERK_SPIELER = 0;
    public static final int LOKALER_SPIELER = 1;
    private Zug zug;
    private int dran;
    private int spielerAnzahl;

    /**
     * Creates a new instance of ComObject
     */
    public ComObject() {
    }

    public void setSpielerAnzahl(int spielerAnzahl) {
        this.spielerAnzahl = spielerAnzahl;
    }

    public int getSpielerNummer() {
        return dran;
    }

    public synchronized Zug getZug() {
        Zug result = zug;
        zug = null;
        return result;

    }

    public synchronized void setZug(Zug zug) {
        //if (spielerNummer != dran) return;
        this.zug = zug;
        //dran = (dran + 1) % spielerAnzahl;
    }
}
