/*
 * Netzwerk.java
 *
 * Created on March 24, 2004, 11:10 AM
 */
package spacehopper.netzwerk;
/**
 * @author sopr023
 */

import spacehopper.core.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.List;

public class NetzwerkClient extends Thread {
    final static String INIT_SIGNAL = "HAPRA";

    private final boolean DEBUG = false;
    private final Spielverwaltung spielverwaltung;
    private final ComObject comObject;
    private final String spielerName;
    private final int spielerTyp;
    private final int clickDelay;
    private final String ip;
    private final Transform transformer;
    private Socket clientSocket;
    private Zug zug;
    private int brettTyp;
    private int spielerNummer;
    private BufferedReader serverBufferedReader;
    private BufferedWriter serverBufferedWriter;
    private boolean laeuft;

    /**
     * Creates a new instance of Netzwerk
     */
    public NetzwerkClient(Spielverwaltung spielverwaltung,
                          ComObject comObject,
                          String spielerName,
                          int spielerTyp,
                          String ip) {
        this.spielverwaltung = spielverwaltung;
        this.comObject = comObject;
        this.spielerName = spielerName;
        this.spielerTyp = spielerTyp;
        this.clickDelay = 200;
        this.ip = ip;
        transformer = new Transform();
    }

    public void run() {
        laeuft = true;

        try {
            clientSocket = new Socket(ip, 1821);

            serverBufferedReader =
                    new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            serverBufferedWriter =
                    new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
        String serverResponse;
        if (empfange().split(" ")[0].equals(INIT_SIGNAL)) {
            sende(spielerName);
            spielerNummer = Integer.parseInt(empfange()) - 1;
            empfange(); // OK emfangen
            String hilf = empfange();
            brettTyp = hilf.equals("Stern") ? 0 : 1;

            while (!empfange().equals("START") && laeuft) {
            }

            int spieleranzahl = Integer.parseInt(empfange());
            String[] spielernamen = new String[spieleranzahl];
            for (int i = 0; i < spieleranzahl; i++) {
                spielernamen[i] = empfange();
                empfange();
            }

            //String mars;
            //while ((mars=empfange()).split(" ")[0].equals("MARS")){}
            int[] typen = new int[spieleranzahl];
            for (int i = 0; i < spieleranzahl; i++) {
                if (i == spielerNummer) {
                    typen[i] = spielerTyp;
                } else {
                    typen[i] = 4;
                }
            }
            comObject.setSpielerAnzahl(spieleranzahl);
            spielverwaltung.newGameStart(typen,
                                         spielernamen,
                                         SpielfeldTyp.byLegacyType(brettTyp)
                                                     .orElse(SpielfeldTyp.QUADRAT));

            while (!clientSocket.isClosed()) {
                serverResponse = empfange();
                if (serverResponse.trim().equals("DRAN")) {
                    while (laeuft) {
                        if ((zug = comObject.getZug()) != null) {
                            sendeZug(zug);
                            break;
                        }
                        try {
                            sleep(50);
                        } catch (Exception e) {
                            if (DEBUG) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else if (serverResponse.split(" ")[0].equals("MARS")
                           && Integer.parseInt(serverResponse.split(" ")[1]) != spielerNummer + 1) {
                    if (empfange().equals("ZUG")) {
                        zug = empfangeZug();

                        List<Position> clicks = spielverwaltung.erreichbar(zug);
                        for (Position click : clicks) {
                            spielverwaltung.klickeFeld(SpielerTyp.NETWORK_PLAYER, click);
                            try {
                                sleep(clickDelay);
                            } catch (Exception e) {
                                if (DEBUG) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        Position position = clicks.get(clicks.size() - 1);
                        spielverwaltung.klickeFeld(SpielerTyp.NETWORK_PLAYER, position);
                    }
                }

                //break;
            }
        }
    }

    private Zug empfangeZug() {
        try {
            int x1 = Integer.parseInt(empfange());
            int y1 = Integer.parseInt(empfange());
            int x2 = Integer.parseInt(empfange());
            int y2 = Integer.parseInt(empfange());

            Position start = (brettTyp == 0)
                             ? transformer.alien2native(x1, y1)
                             : new Position(y1, x1);
            Position end = (brettTyp == 0)
                           ? transformer.alien2native(x2, y2)
                           : new Position(y2, x2);
            return new Zug().erweitertUm(start).erweitertUm(end);
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void sendeZug(Zug zug) {
        try {
            serverBufferedWriter.write(((brettTyp == 0)
                                        ? transformer.native2alien(zug.startpunkt()).x
                                        : zug.startpunkt().y()) + "\n");
            serverBufferedWriter.write(((brettTyp == 0)
                                        ? transformer.native2alien(zug.startpunkt()).y
                                        : zug.startpunkt().x()) + "\n");
            serverBufferedWriter.write(((brettTyp == 0)
                                        ? transformer.native2alien(zug.endpunkt()).x
                                        : zug.endpunkt().y()) + "\n");
            serverBufferedWriter.write(((brettTyp == 0)
                                        ? transformer.native2alien(zug.endpunkt()).y
                                        : zug.endpunkt().x()) + "\n");
            serverBufferedWriter.flush();
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private void sende(String command) {
        try {
            serverBufferedWriter.write(command + "\n");
            serverBufferedWriter.flush();
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private String empfange() {
        try {
            String result = serverBufferedReader.readLine();
            return result;
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void dispose() {
        try {
            clientSocket.close();
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public void beenden() {
        laeuft = false;
        try {
            serverBufferedReader.close();
            serverBufferedWriter.close();
            clientSocket.close();
        } catch (Exception e) {
            if (DEBUG) {
                e.printStackTrace();
            }
        }
    }
}
